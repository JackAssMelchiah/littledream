#ifndef VK_GENERIC_BUFFER_HPP
#define VK_GENERIC_BUFFER_HPP

#include <memory>
#include <vulkan/vulkan.hpp>
#include "Buffer.hpp"

namespace VulkanRenderer {


	class Device;
	
	//! Its not thread safe ya
	class VKGenericBuffer: public Buffer {

	public:
		
		using VRAMRequirements = struct {
			uint32_t size;
			vk::MemoryPropertyFlags flags;
		};

		VKGenericBuffer(Device *device, unsigned int bytesize);

		virtual ~VKGenericBuffer( );
		
		//! Alocates buffer - tries to - to required bytesize - its chunk of continuous memory so large vals can and will fail 
		virtual bool Initialize() override;

		//! Creates all required vulkan info
		virtual bool Build() override;
		
		//! Original vulkan structure for access
		vk::Buffer* operator()() { return &this->vulkanBuffer.get(); }

		//! Basically returns index to memory which can support this buffer's requirements
		int GetRequiredVramIndice(VRAMRequirements& requirements) const;

		//! Clears vulkan part
		virtual void UnBuild() override;

	protected:

		//! Buffer interaction goes against this device
		Device* device;

		//! vulkan buffer object
		vk::UniqueBuffer vulkanBuffer;

		//! alocation
		vk::UniqueDeviceMemory memoryAllocation;

		//!Requirements for VRAM to meet if it want to support this buffer
		VRAMRequirements bufferRequirements;
	};
}




#endif // ! VK_GENERIC_BUFFER_HPP
