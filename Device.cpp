#include "Device.hpp"
#include <SDL_vulkan.h>
#include "Window.hpp"
#include "Shader.hpp"
#include "VKGenericBuffer.hpp"
#include "VKUniformBuffer.hpp"


using namespace VulkanRenderer;

Device::Device(Window* window, std::vector<char const*>const& windowExtensions) :
	
	windowExtensions(windowExtensions),
	wantedValidationLayers({ "VK_LAYER_KHRONOS_validation" }),
	wantedInstanceExtensions({ VK_EXT_DEBUG_UTILS_EXTENSION_NAME }),
	wantedDeviceExtensions({ VK_KHR_SWAPCHAIN_EXTENSION_NAME }),
	window(window), vertexCount(0){

	//initialize que structs, we want ques which are presentation and graphics capable 
	this->availableQues[QueType::QUE_GRAPHICS] = QueWithIndice();
	this->availableQues[QueType::QUE_PRESENTATION] = QueWithIndice();
	this->surfaceFormat = vk::SurfaceFormatKHR{};
	this->surfaceFormat.format = vk::Format::eB8G8R8A8Srgb;
	this->surfaceFormat.colorSpace = vk::ColorSpaceKHR::eVkColorspaceSrgbNonlinear;



	this->vertexPullerSettings = std::make_unique<VertexPullerSettings>();
	this->rasterizerSettings = std::make_unique<RasterizerSettings>();
	this->inputAssemblerSettings = std::make_unique<InputAssemblerSettings>();
	this->viewporScissorSettings = std::make_unique<ViewportSCizzorSettings>(window->dimmensions);
	this->depthStencilSettings = std::make_unique<DepthStencilSettings>();
	this->multisampleSettings = std::make_unique<MultisampleSettings>();
	this->colorBlendSettings = std::make_unique<ColorBlendSettings>();
}



Device::~Device() {

	this->device->waitIdle();
	this->descriptorSets.clear();
}


/* Sets buffer to use within renderer, provides info about how many times vertex shaders gets executed*/
void VulkanRenderer::Device::SetBuffer(VKGenericBuffer* buffer, uint32_t vertexCount){

	this->vertexBuffer = buffer;
	this->vertexCount = vertexCount;
}


/* Sets unifor buffer */
void VulkanRenderer::Device::SetBuffer(VKUniformBuffer* buffer){

	this->uniformBuffer = buffer;
}


/* Prepares vulkan*/
bool Device::_Build() {

	//for all of theese things there is no need to recreate them
	//every method here needs to have logical device created at this point
	auto shaderResources = std::vector<Shader::ShaderSource>({ {std::string(SHADER_DIR) +"spirv/explicitTriangle.vert.spv", Shader::ShaderType::VERTEX }, {std::string(SHADER_DIR) + "spirv/explicitTriangle.frag.spv", Shader::ShaderType::FRAGMENT} });
	auto shaders = Shader(shaderResources);
	if (!shaders.LoadShaders()) {	
		return false;
	}
	NewShaderStage(shaders, this->shadersInfo, this->shaderModules);
	if (this->shadersInfo.empty())
		return false;
	
	DescriptorSetLayout();

	DescriptorPool();

	CommandPool();
	Semaphores();

	//all other stuff happens here - all things that need to be re-created when swapchain is modified
	RefreshSwapchain(this->shadersInfo, nullptr);

	return true;
}


/* Creates a logical device, which then vulkan uses for everything. Logical device is abstraction on your favorite vulkan capable chip - mostly some gpu */
bool Device::_Initialize() {


	auto appInfoStruct(vk::ApplicationInfo{}); {
		appInfoStruct.pApplicationName = this->appName.c_str();
		appInfoStruct.pEngineName = this->engineName.c_str();
		appInfoStruct.applicationVersion = VK_MAKE_VERSION(this->appVersion.x, this->appVersion.y, this->appVersion.z);
		appInfoStruct.engineVersion = VK_MAKE_VERSION(this->engineVersion.x, this->engineVersion.y, this->engineVersion.z);
		appInfoStruct.apiVersion = VK_MAKE_VERSION(1, 0, 0);
	}

	//get required extesions and combine them with extensions from window 
	auto requiredInstanceExtensions = std::vector<char const*>();
	GetInstanceExtensions(requiredInstanceExtensions);
	
	//get validation layers
	auto requiredValidationLayers = std::vector<char const*>();
	GetValidationLayers(requiredValidationLayers);

	auto instanceInfoStruct(vk::InstanceCreateInfo{}); {
		instanceInfoStruct.enabledExtensionCount = requiredInstanceExtensions.size();
		instanceInfoStruct.ppEnabledExtensionNames = requiredInstanceExtensions.data();
		instanceInfoStruct.enabledLayerCount = requiredValidationLayers.size();
		instanceInfoStruct.ppEnabledLayerNames = requiredValidationLayers.data();
		instanceInfoStruct.pApplicationInfo = &appInfoStruct;
	}
	//Build VKInstance
	this->instance = vk::createInstanceUnique(instanceInfoStruct);

	//build surface on window for vulkan to draw on
	auto surface = VkSurfaceKHR();
	if (!SDL_Vulkan_CreateSurface(this->window->window, this->instance.get().operator VkInstance(), &surface)) {
		assert(!"Unable to create surface for vulkan to draw on from window");
		return false;
	}

	this->windowSurface = vk::UniqueSurfaceKHR{ surface, this->instance.get() };

	//Find suitable HW devices, and make Devices(logical) from them
	auto availablePhysicalDevices = this->instance->enumeratePhysicalDevices();

	//It is required that theese features supported on device, since they will be used
	auto requiredDeviceFeatures(vk::PhysicalDeviceFeatures{}); {
		requiredDeviceFeatures.multiDrawIndirect = VK_TRUE;
	}


	auto infoStr = std::string("Available devices:\n");

	//pick the best device for given needs - get some discrete gpu
	for (auto& currentPhysicalDevice : availablePhysicalDevices) {

		auto properties = currentPhysicalDevice.getProperties();
		auto features = currentPhysicalDevice.getFeatures();
		auto memoryProperties = currentPhysicalDevice.getMemoryProperties();
		auto queFamilyProperties = currentPhysicalDevice.getQueueFamilyProperties();

		auto physicalDeviceCapable = DeviceExtensionsSupported(currentPhysicalDevice, this->wantedDeviceExtensions);

		//if its discrete gpu, has all required extension support, lets take a look into queues and look if there are all that are needed (eq. device supports everything we need)
		if (physicalDeviceCapable && properties.deviceType == vk::PhysicalDeviceType::eDiscreteGpu && features.multiDrawIndirect) {
			
			//check for window rendering things
			auto deviceWindowSupport = false;
			auto surfaceCapabilites = currentPhysicalDevice.getSurfaceCapabilitiesKHR(this->windowSurface.get());
			auto surfaceFormats = currentPhysicalDevice.getSurfaceFormatsKHR(this->windowSurface.get());
			auto surfacePresentModes = currentPhysicalDevice.getSurfacePresentModesKHR(this->windowSurface.get());
			
			//format check - want to use srgba 8bit
			for (auto& availableFormat : surfaceFormats) {
				if (availableFormat.format == this->surfaceFormat.format && availableFormat.colorSpace == this->surfaceFormat.colorSpace) {
					deviceWindowSupport = true;
				}
			}
			if (deviceWindowSupport) {
				auto queIndex = 0;
				for (auto& queFamilyProperty : queFamilyProperties) {
					//descrete device we are looking for needs to support triangle draw, etc..
					if ((queFamilyProperty.queueFlags & vk::QueueFlagBits::eGraphics) && (queFamilyProperty.queueCount >= 1)) {
						this->physicalDevice = currentPhysicalDevice;
						this->availableQues[QueType::QUE_GRAPHICS].indice = queIndex; //this nasty thing gets iteration index

					}
					if (this->physicalDevice.getSurfaceSupportKHR(queIndex, this->windowSurface.get())) {
						this->availableQues[QueType::QUE_PRESENTATION].indice = queIndex;
					}
					++queIndex;
				}
			}
		}

		infoStr += "\t" + std::string(properties.deviceName) + "\n";
	}
	if (!this->availableQues.count(QueType::QUE_GRAPHICS) || !this->availableQues.count(QueType::QUE_PRESENTATION)) {
		assert(!"Was not able to select physical device according to specified preferences");
		return false;
	}


	//just print what was found and selected
	auto driverVersion = this->physicalDevice.getProperties().driverVersion;
	infoStr += "Will be using " + std::string(this->physicalDevice.getProperties().deviceName) \
		+ ", Driver version:" + std::to_string(VK_VERSION_MAJOR(driverVersion))\
		+ "." + std::to_string(VK_VERSION_MINOR(driverVersion))\
		+ "." + std::to_string(VK_VERSION_PATCH(driverVersion));

	Utils::PrintOut(infoStr);

	auto queMaxPriority = 1.f;
	auto queueInfoStructs = std::vector<vk::DeviceQueueCreateInfo>(); {
		for (auto& selectedQueFamilyIndex : this->availableQues) {
			auto queueInfoStruct(vk::DeviceQueueCreateInfo{}); {
				queueInfoStruct.pQueuePriorities = &queMaxPriority;
				queueInfoStruct.queueCount = 1;
				queueInfoStruct.queueFamilyIndex = selectedQueFamilyIndex.second.indice;
				queueInfoStructs.push_back(queueInfoStruct);
			}
		}
	}


	auto deviceInfoStruct(vk::DeviceCreateInfo{}); {
		deviceInfoStruct.enabledExtensionCount = this->wantedDeviceExtensions.size(); //it is known that wanted device extensions are available
		deviceInfoStruct.ppEnabledExtensionNames = this->wantedDeviceExtensions.data();
		deviceInfoStruct.enabledLayerCount = requiredValidationLayers.size();
		deviceInfoStruct.ppEnabledLayerNames = requiredValidationLayers.data();
		deviceInfoStruct.queueCreateInfoCount = queueInfoStructs.size();
		deviceInfoStruct.pQueueCreateInfos = queueInfoStructs.data();
		deviceInfoStruct.pEnabledFeatures = &requiredDeviceFeatures;
	}

	//and create logical device from given physical device - note, multiple logical devices can be created from physical device
	this->device = this->physicalDevice.createDeviceUnique(deviceInfoStruct);
	
	//load wanted ques from device
	for (auto& wantedQue : this->availableQues) {
		wantedQue.second.que = this->device->getQueue(wantedQue.second.indice, 0); // get the first within specified family
	}



	return true;
}



void Device::GetValidationLayers(std::vector<const char*>& validationLayers) const {

	validationLayers.clear();
	
	auto availableLayers = vk::enumerateInstanceLayerProperties();
	//lets see if wanted layers are available on this system and return them
	for (auto& wantedValidationLayer : this->wantedValidationLayers) {
		auto wantedLayerFound = false;
		for (auto& availableLayer : availableLayers) {
			if (std::string(availableLayer.layerName) == std::string(wantedValidationLayer)) {
				validationLayers.push_back(wantedValidationLayer);
				wantedLayerFound = true;
			}
		}
		if (!wantedLayerFound) {
			Utils::PrintOutWar("Validation layer " + std::string(wantedValidationLayer) + "wont be used, as it was not found");
		}
	}
}



void Device::GetInstanceExtensions(std::vector<char const*>& extensions) const {

	extensions.clear();
	auto availableExtensions = vk::enumerateInstanceExtensionProperties();
	auto wantedExtensions = std::vector<char const*>();
	
	//check both instance extensions and window extensions
	wantedExtensions.insert(wantedExtensions.end(), this->wantedInstanceExtensions.begin(), this->wantedInstanceExtensions.end());
	wantedExtensions.insert(wantedExtensions.end(), this->windowExtensions.begin(), this->windowExtensions.end());

	//lets see if wanted extensions are available on this system and return them
	for (auto& wantedExtension : wantedExtensions) {
		auto wantedExtensionFound = false;
		for (auto& availableExtension : availableExtensions) {
			if (std::string(availableExtension.extensionName) == std::string(wantedExtension)) {
				extensions.push_back(wantedExtension);
				wantedExtensionFound = true;
				break;
			}
		}
		if (!wantedExtensionFound) {
			Utils::PrintOutWar("Extension " + std::string(wantedExtension) + "wont be used, as it was not found");
		}
	}
}



bool Device::DeviceExtensionsSupported(vk::PhysicalDevice& device, std::vector<char const*>& extensions) const {

	auto availableDeviceExtensions = device.enumerateDeviceExtensionProperties();
	//lets see if wanted layers are available on this system and return them
	for (auto& wantedDeviceExtension : extensions) {
		auto wantedDeviceExtensionFound = false;
		for (auto& availableDeviceExtension : availableDeviceExtensions) {
			if (std::string(availableDeviceExtension.extensionName) == std::string(wantedDeviceExtension)) {
				wantedDeviceExtensionFound = true;
			}
		}
		if (!wantedDeviceExtensionFound) {
			return false;
		}
	}
	return true;
}


void Device::Swapchain(vk::SwapchainKHR* oldSwapchain) {


	auto imageCount = 3; //tripple buffering

	auto queFamilyIndices = std::vector<Uint32>({this->availableQues.at(QueType::QUE_GRAPHICS).indice, this->availableQues.at(QueType::QUE_PRESENTATION).indice });
	auto queFamilySharingMode = vk::SharingMode::eConcurrent;

	//if both ques are from the same family, then resources does need to be shared - faster
	if (this->availableQues.at(QueType::QUE_GRAPHICS).indice == this->availableQues.at(QueType::QUE_PRESENTATION).indice) {
		queFamilySharingMode = vk::SharingMode::eExclusive;
		queFamilyIndices.clear();
	}
	auto surfaceCapabilites = this->physicalDevice.getSurfaceCapabilitiesKHR(this->windowSurface.get());

	auto drawableArea = vk::Rect2D();
	this->window->GetRect(drawableArea);

	auto swapChainInfoStruct(vk::SwapchainCreateInfoKHR{}); {
		swapChainInfoStruct.surface = this->windowSurface.get();
		swapChainInfoStruct.minImageCount = imageCount; //tripple buffering
		swapChainInfoStruct.imageFormat = this->surfaceFormat.format;
		swapChainInfoStruct.imageColorSpace = vk::ColorSpaceKHR::eVkColorspaceSrgbNonlinear;
		swapChainInfoStruct.imageExtent = drawableArea.extent;
		swapChainInfoStruct.imageArrayLayers = 1; //allways 1
		swapChainInfoStruct.imageUsage = vk::ImageUsageFlagBits::eColorAttachment; //directly render into them, no postprocessing
		swapChainInfoStruct.queueFamilyIndexCount = queFamilyIndices.size();
		swapChainInfoStruct.pQueueFamilyIndices = queFamilyIndices.data();
		swapChainInfoStruct.imageSharingMode = queFamilySharingMode;
		swapChainInfoStruct.preTransform = surfaceCapabilites.currentTransform; // please dont apply any transform to image, keep it as it is
		swapChainInfoStruct.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque; //ignore any blending with other windows
		swapChainInfoStruct.presentMode = vk::PresentModeKHR::eFifo; //guaranteed to exist, its v-sync
		swapChainInfoStruct.clipped = VK_TRUE; //allow to clip when other window is in front
		swapChainInfoStruct.oldSwapchain = oldSwapchain == nullptr ? nullptr: *oldSwapchain; //use old swapchain as cache if present
	}


	this->swapchain = this->device->createSwapchainKHRUnique(swapChainInfoStruct);
}



void Device::SwapchainImageView() {

	//swapchain basically just exposes images for the render, their count is specified by minImageCount in SwapchainCreateInfoKHR
	//there needs to be specified format for those images (like with any other images)

	//retrieve images from swapchain
	auto swapchainImages = this->device->getSwapchainImagesKHR(this->swapchain.get());
	//create view for every image retrieved
	this->swapchainImageViews.clear();
	this->swapchainImageViews.resize(swapchainImages.size());

	for (auto i = 0; i < swapchainImages.size(); ++i) {
		auto imageViewInfoStruct(vk::ImageViewCreateInfo{}); {
				
			imageViewInfoStruct.format = this->surfaceFormat.format;
			imageViewInfoStruct.image = swapchainImages.at(i);
			imageViewInfoStruct.viewType = vk::ImageViewType::e2D;
			//just let rgba mappings to be 0-1
			imageViewInfoStruct.components.r = vk::ComponentSwizzle::eIdentity;
			imageViewInfoStruct.components.g = vk::ComponentSwizzle::eIdentity;
			imageViewInfoStruct.components.b = vk::ComponentSwizzle::eIdentity;
			imageViewInfoStruct.components.a = vk::ComponentSwizzle::eIdentity;
			//just one layer, no 3d
			imageViewInfoStruct.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eColor;
			imageViewInfoStruct.subresourceRange.baseArrayLayer = 0;
			imageViewInfoStruct.subresourceRange.layerCount = 1;
			imageViewInfoStruct.subresourceRange.baseMipLevel = 0;
			imageViewInfoStruct.subresourceRange.levelCount = 1;
		}
		this->swapchainImageViews[i] = this->device->createImageViewUnique(imageViewInfoStruct);
	}
}


/*Builds up a new shader stage, note that shader modules are build as part of this and for reuse one might want to store them*/
void Device::NewShaderStage(VulkanRenderer::Shader const& shader, std::vector<vk::PipelineShaderStageCreateInfo>& shaderStageInfoStructs, std::vector<vk::UniqueShaderModule>& shaderModules) {

	auto shaderInputs = shader.GetShaderBinaries();
	shaderStageInfoStructs.clear();
	shaderModules.clear();
	shaderStageInfoStructs.resize(shaderInputs.size());
	shaderModules.resize(shaderInputs.size());

	for (auto i = 0; i < shaderInputs.size(); ++ i) {
		//firs build shader module - just specify loaded source binary for vulkan
		auto shaderModuleInfoStruct(vk::ShaderModuleCreateInfo{});
		shaderModuleInfoStruct.pCode = reinterpret_cast<const uint32_t*>(shaderInputs.at(i).srcCode.data());
		shaderModuleInfoStruct.codeSize = shaderInputs.at(i).srcCode.size();
		

		//Dont need to save modules since reuse wont be nescessary
		shaderModules[i] = this->device->createShaderModuleUnique(shaderModuleInfoStruct);
		
		//now add adinational info aboout shader - what kind of shader it is, entry point.
		auto& shaderStageInfoStruct = shaderStageInfoStructs[i];
			shaderStageInfoStruct.module = shaderModules[i].get();
			shaderStageInfoStruct.stage = shaderInputs.at(i).shaderTypeVK;
			shaderStageInfoStruct.pName = "main";
			
	}
}


void Device::PipelineLayout(){
	
	auto pipelineLayoutInfoStruct(vk::PipelineLayoutCreateInfo{}); {
		pipelineLayoutInfoStruct.pushConstantRangeCount = 0;
		pipelineLayoutInfoStruct.pPushConstantRanges = nullptr;
		pipelineLayoutInfoStruct.setLayoutCount = 1;
		pipelineLayoutInfoStruct.pSetLayouts = &this->descriptorSetLayout.get();
	}
	
	this->pipelineLayout = this->device->createPipelineLayoutUnique(pipelineLayoutInfoStruct);
}


void Device::RenderPass() {

	//use single subpass - but even single pass must use framebuffer - thats basically output of fragment shader - so lets tell vulkan about it

	//basically output attachement - rendering into framebuffer in fragment shader at bindPoint 0
	auto attachements(vk::AttachmentReference{}); {
		attachements.attachment = 0;
		attachements.layout = vk::ImageLayout::eColorAttachmentOptimal; //attachement is color framebuffer 
	}

	
	auto subpassDesc(vk::SubpassDescription{}); {
		subpassDesc.colorAttachmentCount = 1;
		subpassDesc.pColorAttachments = &attachements;
		subpassDesc.pipelineBindPoint = vk::PipelineBindPoint::eGraphics; //must be explicit
	}

	auto attachmentDesc(vk::AttachmentDescription{}); {
		attachmentDesc.format = this->surfaceFormat.format; //rendering into swapchain image, therefore swapchain image format
		attachmentDesc.samples = vk::SampleCountFlagBits::e1; // no multisampling
		attachmentDesc.loadOp = vk::AttachmentLoadOp::eClear; // attachements should be cleared before rendering
		attachmentDesc.storeOp = vk::AttachmentStoreOp::eStore; // after rendeing, store what was rendered please!
		attachmentDesc.stencilLoadOp = vk::AttachmentLoadOp::eDontCare;
		attachmentDesc.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
		attachmentDesc.initialLayout = vk::ImageLayout::eUndefined;
		attachmentDesc.finalLayout = vk::ImageLayout::ePresentSrcKHR; //this image is to be used for swapchain
	}

	//subpass dependency - swapchain image is not acquired when renderpass begin so this needs to be specified in dependency
	auto dependency(vk::SubpassDependency{}); {
		dependency.srcSubpass = VK_SUBPASS_EXTERNAL; //refers to implicit subpass which 
		dependency.dstSubpass = 0; //
		dependency.srcStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
		dependency.srcAccessMask = vk::AccessFlags::Flags(); // no flags
		dependency.dstAccessMask = vk::AccessFlagBits::eColorAttachmentWrite;
		dependency.dstStageMask = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	}

	//put it all together
	auto renderPassInfoStruct(vk::RenderPassCreateInfo{}); {
		renderPassInfoStruct.attachmentCount = 1;
		renderPassInfoStruct.pAttachments = &attachmentDesc;
		renderPassInfoStruct.dependencyCount = 1;
		renderPassInfoStruct.pDependencies = &dependency;
		renderPassInfoStruct.subpassCount = 1;
		renderPassInfoStruct.pSubpasses = &subpassDesc;
	}

	this->renderPass = this->device->createRenderPassUnique(renderPassInfoStruct);
	
}


/* Creates pipeline  */
void Device::Pipeline(std::vector<vk::PipelineShaderStageCreateInfo>& shaderStages) {

	auto colorBlendState = vk::PipelineColorBlendStateCreateInfo{};
	auto colorBlendAttachement = vk::PipelineColorBlendAttachmentState{};

	this->colorBlendSettings->get(colorBlendAttachement, colorBlendState);


	//get all specified states and prepare them together for pipeline
	auto pipelineInfoStruct(vk::GraphicsPipelineCreateInfo{}); {
		pipelineInfoStruct.basePipelineHandle = nullptr;
		pipelineInfoStruct.basePipelineIndex = -1; //dont use 
		pipelineInfoStruct.stageCount = shaderStages.size();
		pipelineInfoStruct.pStages = shaderStages.data();
		pipelineInfoStruct.layout = *this->pipelineLayout;
		pipelineInfoStruct.pColorBlendState = &colorBlendState;
		pipelineInfoStruct.pDepthStencilState = nullptr;
		pipelineInfoStruct.pDynamicState = nullptr;
		pipelineInfoStruct.pInputAssemblyState = &this->inputAssemblerSettings->get();
		pipelineInfoStruct.pVertexInputState = &this->vertexPullerSettings->get();
		pipelineInfoStruct.pMultisampleState = &this->multisampleSettings->get();
		pipelineInfoStruct.pRasterizationState = &this->rasterizerSettings->get();
		pipelineInfoStruct.pTessellationState = nullptr;
		pipelineInfoStruct.pViewportState = &this->viewporScissorSettings->get();
		pipelineInfoStruct.renderPass = this->renderPass.get();
		pipelineInfoStruct.subpass = 0; //its just first subpass
		
	}

	this->pipeline = this->device->createGraphicsPipelineUnique(nullptr, pipelineInfoStruct); //dont use pipeline cache
}


/* Creates default framebuffer - note that every swapchain image must have one */
void Device::DefaultFramebuffer() {

	auto drawableArea = vk::Rect2D();
	this->window->GetRect(drawableArea);

	this->defaultFramebuffer.clear();
	this->defaultFramebuffer.resize(this->swapchainImageViews.size());

	for (auto i = 0; i < this->defaultFramebuffer.size(); ++i) {
		auto framebufferInfoStruct(vk::FramebufferCreateInfo{}); {
			framebufferInfoStruct.attachmentCount = 1;
			framebufferInfoStruct.pAttachments = &this->swapchainImageViews[i].get();
			framebufferInfoStruct.layers = 1;
			framebufferInfoStruct.width = drawableArea.extent.width;
			framebufferInfoStruct.height = drawableArea.extent.height;
			framebufferInfoStruct.renderPass = this->renderPass.get();
		}
		this->defaultFramebuffer[i] = this->device->createFramebufferUnique(framebufferInfoStruct);
	}
}


/* Builds up command pool for graphics capable que*/
void Device::CommandPool(){

	auto commandPoolInfoStruct(vk::CommandPoolCreateInfo{}); {
		commandPoolInfoStruct.queueFamilyIndex = this->availableQues.at(QueType::QUE_GRAPHICS).indice;
	}

	this->commandPool = this->device->createCommandPoolUnique(commandPoolInfoStruct);
}


/* Alocates one command buffer per swapchain image */
void Device::CommandBuffer() {

	auto commandBufferAllocateStruct(vk::CommandBufferAllocateInfo{}); {
		commandBufferAllocateStruct.commandBufferCount = this->swapchainImageViews.size();
		commandBufferAllocateStruct.commandPool = this->commandPool.get();
		commandBufferAllocateStruct.level = vk::CommandBufferLevel::ePrimary;// - submitable to que for execution
	}

	this->commandBuffer = this->device->allocateCommandBuffersUnique(commandBufferAllocateStruct);
}


/* Records all commandbuffers for swapchainImages */
void Device::RecordRenderPass() {

	for (int swapchainImageIndex = 0; swapchainImageIndex < this->swapchainImageViews.size(); ++swapchainImageIndex) {

		//clearing color
		auto clearColor = vk::ClearValue(); {
			clearColor.color = vk::ClearColorValue{ std::array<float, 4>{55 / 255.f, 60 / 255.f, 63 / 255.f, 1.f} };
		}

		auto drawableArea = vk::Rect2D();
		this->window->GetRect(drawableArea);

		//renderpass info - specifies what clear color it will use and what framebuffer - also render area for tiled rendering
		auto renderPassBeginInfoStruct(vk::RenderPassBeginInfo{}); {
			renderPassBeginInfoStruct.framebuffer = this->defaultFramebuffer[swapchainImageIndex].get();
			renderPassBeginInfoStruct.clearValueCount = 1;
			renderPassBeginInfoStruct.pClearValues = &clearColor;
			renderPassBeginInfoStruct.renderArea = drawableArea;
			renderPassBeginInfoStruct.renderPass = this->renderPass.get();
		}

		auto commandBufferBeginInfoStruct(vk::CommandBufferBeginInfo{}); {
			commandBufferBeginInfoStruct.pInheritanceInfo = nullptr; //which state to inherit from - useful for secondary command buffers
		}

		//start recording command buffers - resets command buffer to initial state
		this->commandBuffer.at(swapchainImageIndex)->begin(commandBufferBeginInfoStruct);

		//command buffer has render pass embeded in primary command buffer, no secondary command buffers will be executed
		this->commandBuffer.at(swapchainImageIndex)->beginRenderPass(renderPassBeginInfoStruct, vk::SubpassContents::eInline);
		//use specified graphics pipeline that was created earlier in this renderpass
		this->commandBuffer.at(swapchainImageIndex)->bindPipeline(vk::PipelineBindPoint::eGraphics, this->pipeline.get());
		//bind vertex buffer
		auto firstBindPoint = uint32_t(0);

		auto buffersToBind = vk::ArrayProxy<const vk::Buffer>{ *this->vertexBuffer->operator()(), *this->vertexBuffer->operator()() };
		// 1 vbo - 2attribs - color starts after vertices
		auto colorOffset = this->vertexCount * sizeof(glm::vec3);
		std::vector<vk::DeviceSize> offsets{ 0,  colorOffset };
		auto bufferOffsets = vk::ArrayProxy<vk::DeviceSize const>(offsets);

		this->commandBuffer.at(swapchainImageIndex)->bindVertexBuffers(firstBindPoint, buffersToBind, bufferOffsets);

		//bind descriptorSets - just bind one ubo
		auto offset = uint32_t(0); //first ubo starts at 0 bytes offset
		this->commandBuffer.at(swapchainImageIndex)->bindDescriptorSets(
			vk::PipelineBindPoint::eGraphics,
			this->pipelineLayout.get(),
			0, //start with first descriptor set...
			vk::ArrayProxy<vk::DescriptorSet const>(this->descriptorSets.at(swapchainImageIndex).get()),
			vk::ArrayProxy<uint32_t const>(nullptr)			
		);

		//render
		assert(this->vertexCount > 0);

		this->commandBuffer.at(swapchainImageIndex)->draw(this->vertexCount, 1, 0, 0);
		//end renderpass
		this->commandBuffer.at(swapchainImageIndex)->endRenderPass();
		//since only one renderpass is used, end record of command buffer
		this->commandBuffer.at(swapchainImageIndex)->end();
	}
}


/* Builds semaphores, one is used for when rendering is done, the other for swapchain acquisition */
void Device::Semaphores() {

	auto semaphoreInfotruct(vk::SemaphoreCreateInfo{}); {
		//empty, for future versions maybe something changes
	}

	this->swapchainSemaphore = this->device->createSemaphoreUnique(semaphoreInfotruct);
	this->renderingSemaphore = this->device->createSemaphoreUnique(semaphoreInfotruct);
}


void Device::Render() {

	//get image from swapchain
	auto imageIndex = this->device->acquireNextImageKHR(this->swapchain.get(), UINT64_MAX, this->swapchainSemaphore.get(), nullptr).value;

	//stage when semaphore should be signalized is when image is done after fragment shader
	auto wantedStage = vk::PipelineStageFlags(vk::PipelineStageFlagBits::eColorAttachmentOutput);

	//submit command buffer to que
	auto submitInfoStruct(vk::SubmitInfo{}); {
		submitInfoStruct.commandBufferCount = 1;
		submitInfoStruct.pCommandBuffers = &this->commandBuffer.at(imageIndex).get(); // command which is "designated" to image in swapchain
		submitInfoStruct.signalSemaphoreCount = 1;
		submitInfoStruct.pSignalSemaphores = &this->renderingSemaphore.get(); //this semaphore will be signalized when specified stage is reached
		submitInfoStruct.waitSemaphoreCount = 1;
		submitInfoStruct.pWaitSemaphores = &this->swapchainSemaphore.get(); //wait on whic semafore before execution begins
		submitInfoStruct.pWaitDstStageMask = &wantedStage;
	}

	auto submitInfoStructs = vk::ArrayProxy<vk::SubmitInfo const>(1, &submitInfoStruct);
	
	//submit que 
	this->availableQues.at(QueType::QUE_GRAPHICS).que.submit(submitInfoStructs, nullptr); //fence  - signalize when its complete

	
	auto presentInfo(vk::PresentInfoKHR{});{
		presentInfo.waitSemaphoreCount = 1; //wait for rendering semaphore which notifies when rendering was completed
		presentInfo.pWaitSemaphores = &this->renderingSemaphore.get();
		presentInfo.swapchainCount = 1; //use defined swapchain, maybe you could want more if there were multiple windows to draw to
		presentInfo.pSwapchains = &this->swapchain.get();
		presentInfo.pImageIndices = &imageIndex; //write into given image
	}


	this->availableQues.at(QueType::QUE_GRAPHICS).que.presentKHR(presentInfo);

	this->availableQues.at(QueType::QUE_GRAPHICS).que.waitIdle(); //waits till this que has done its work
}


/* When window was resized, all things dependent on swapchain needs to be recreated */
void Device::Resize(){

	RefreshSwapchain(this->shadersInfo, &this->swapchain.get());
}


/* All dependent operation when recreating swapchain are called here */
void Device::RefreshSwapchain(std::vector<vk::PipelineShaderStageCreateInfo>& shaderInfoStructs, vk::SwapchainKHR* oldSwapchain) {

	this->device->waitIdle();

	Swapchain(oldSwapchain);

	SwapchainImageView();



	DescriptorSet(3 * sizeof(glm::mat4)); //3 matrices go to the UBO

	RenderPass();

	PipelineLayout();

	auto windowRect = vk::Rect2D();
	this->window->GetRect(windowRect);

	//viewport size, which is later used in pipeline should be the same as is window 
	this->viewporScissorSettings->set(glm::ivec2(windowRect.extent.width, windowRect.extent.height));
	
	Pipeline(shaderInfoStructs);

	DefaultFramebuffer();

	CommandBuffer();

	RecordRenderPass();
}


void Device::DescriptorPool() {

	auto descriptorCount = 3;//this->swapchainImageViews.size();
	auto poolSize(vk::DescriptorPoolSize{});{
		poolSize.descriptorCount = descriptorCount; //need one descriptor per swapchain image
		poolSize.type = vk::DescriptorType::eUniformBuffer;
	}

	auto descriptorPoolInfoStruct(vk::DescriptorPoolCreateInfo{}); {
		descriptorPoolInfoStruct.poolSizeCount = 1; //one pool
		descriptorPoolInfoStruct.pPoolSizes = &poolSize;
		descriptorPoolInfoStruct.maxSets = descriptorCount; //max that will be possibly allocated
		descriptorPoolInfoStruct.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet; //if one is to use unique ptrs, which will try to free descriptor set, this flag is mandatory
	} 

	this->descriptorPool = this->device->createDescriptorPoolUnique(descriptorPoolInfoStruct);
}


void Device::DescriptorSetLayout() {

	//describe uniform buffer
	auto uboDescriptorBinding(vk::DescriptorSetLayoutBinding{}); {
		uboDescriptorBinding.binding = 0; //bindpoint in shader goes to slot 0
		uboDescriptorBinding.descriptorCount = 1; //if uniform in buffer were an array, this is where would one specify its length
		uboDescriptorBinding.descriptorType = vk::DescriptorType::eUniformBuffer; //its uniform buffer
		uboDescriptorBinding.stageFlags = vk::ShaderStageFlagBits::eVertex; //ubo will be accessible in vertex shader only
		uboDescriptorBinding.pImmutableSamplers = nullptr; //dont use samplers...
	}

	auto descriptorSetLayoutInfoStruct(vk::DescriptorSetLayoutCreateInfo{}); {
		descriptorSetLayoutInfoStruct.bindingCount = 1;
		descriptorSetLayoutInfoStruct.pBindings = &uboDescriptorBinding;
	}

	this->descriptorSetLayout = this->device->createDescriptorSetLayoutUnique(descriptorSetLayoutInfoStruct);
}


/*Allocated new descriptor set*/
void Device::DescriptorSet(unsigned int descriptorByteSize) {

	this->descriptorSets.clear(); //clear old sets, otherwise will fail since max allocated descriptors was specified to be 3

	auto layouts = std::vector<vk::DescriptorSetLayout>(this->swapchainImageViews.size(), this->descriptorSetLayout.get());

	//alocation description
	auto descAllocInfoStruct(vk::DescriptorSetAllocateInfo{}); {
		descAllocInfoStruct.descriptorPool = this->descriptorPool.get(); //use this pool
		descAllocInfoStruct.descriptorSetCount = this->swapchainImageViews.size(); //will be same number of descriptors as swapchain images
		descAllocInfoStruct.pSetLayouts = layouts.data(); //prepared layout - its same per descriptor
	}
	this->descriptorSets = this->device->allocateDescriptorSetsUnique(descAllocInfoStruct);

	auto writeInfo = std::vector<vk::WriteDescriptorSet>(this->swapchainImageViews.size(), vk::WriteDescriptorSet{});

	for (auto i = 0; i < this->swapchainImageViews.size(); ++i) {
		auto descBufferInfoStruct(vk::DescriptorBufferInfo{}); {
			descBufferInfoStruct.buffer = *this->uniformBuffer->operator()(i); //its for this buffer
			descBufferInfoStruct.offset = 0; //going with three buffers, so each starts at the beggining - could have used one buffer with different offsets
			descBufferInfoStruct.range = descriptorByteSize; // uniform size
		}

		writeInfo[i].descriptorCount = 1;
		writeInfo[i].descriptorType = vk::DescriptorType::eUniformBuffer;
		writeInfo[i].dstArrayElement = 0; //descriptor is not an array, this should turn it off
		writeInfo[i].dstSet = this->descriptorSets.at(i).get();
		writeInfo[i].pBufferInfo = &descBufferInfoStruct;
		writeInfo[i].pImageInfo = nullptr; //no descriptor refers to image data
		writeInfo[i].pTexelBufferView = nullptr;
	}

	//applies descriptor setting
	this->device->updateDescriptorSets(vk::ArrayProxy<vk::WriteDescriptorSet const>(writeInfo), vk::ArrayProxy<vk::CopyDescriptorSet const>(nullptr));
}
