#ifndef _FIXED_PIPELINE_SETTINGS_
#define _FIXED_PIPELINE_SETTINGS_

#include <vulkan/vulkan.hpp>
#include <glm.hpp>

namespace VulkanRenderer {

    class VertexPullerSettings {

    public:
        VertexPullerSettings();
        ~VertexPullerSettings() {}

        vk::PipelineVertexInputStateCreateInfo& get() { return this->vertexPullerInfoStruct; }

    protected:

        std::vector<vk::VertexInputBindingDescription> bindingDesc;

        std::vector<vk::VertexInputAttributeDescription> attribDesc;

        vk::PipelineVertexInputStateCreateInfo vertexPullerInfoStruct;
    };


    class RasterizerSettings {
    public:
        RasterizerSettings();
        ~RasterizerSettings() {}

        vk::PipelineRasterizationStateCreateInfo& get() { return this->rasterizerInfoStruct; }

    protected:

        vk::PipelineRasterizationStateCreateInfo rasterizerInfoStruct;
    };


    class InputAssemblerSettings {
    public:
        InputAssemblerSettings();
        ~InputAssemblerSettings() {}

        vk::PipelineInputAssemblyStateCreateInfo& get() { return this->inputAssemblyInfoStruct; }

    protected:

        vk::PipelineInputAssemblyStateCreateInfo inputAssemblyInfoStruct;
    };


    class ViewportSCizzorSettings {
    public:
        ViewportSCizzorSettings(glm::ivec2 resolution);
        ~ViewportSCizzorSettings() {}

        vk::PipelineViewportStateCreateInfo& get() { return this->viewportScissorInfoStruct; }
        void set(glm::ivec2 resoulution);

    protected:

        vk::PipelineViewportStateCreateInfo viewportScissorInfoStruct;

        vk::Rect2D scissorRect;

        vk::Viewport viewport;
    };


    class DepthStencilSettings {

    public:
        DepthStencilSettings();
        ~DepthStencilSettings() {}

        vk::PipelineDepthStencilStateCreateInfo& get() { return this->depthStencilInfoStruct; }

    protected:

        vk::PipelineDepthStencilStateCreateInfo depthStencilInfoStruct;
    };


    class MultisampleSettings {

    public:
        MultisampleSettings();
        ~MultisampleSettings() {}

        vk::PipelineMultisampleStateCreateInfo& get() { return this->multisampleInfoStruct; }

    protected:

        vk::PipelineMultisampleStateCreateInfo multisampleInfoStruct;
    };


    class ColorBlendSettings {

    public:
        ColorBlendSettings();
        ~ColorBlendSettings() {}

        void get(vk::PipelineColorBlendAttachmentState& blendAttachments, vk::PipelineColorBlendStateCreateInfo& blendState) {
            blendAttachments = this->colorBlendAttachmentsInfoStruct; blendState = this->colorBlendStateInfoStruct; 
        }

    protected:

        vk::PipelineColorBlendAttachmentState colorBlendAttachmentsInfoStruct;
        vk::PipelineColorBlendStateCreateInfo colorBlendStateInfoStruct;
    };


}








#endif // !_FIXED_PIPELINE_SETTINGS_
