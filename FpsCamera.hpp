#ifndef FPS_CAMERA_HPP
#define FPS_CAMERA_HPP

#include "Camera.hpp"
#include "Utils.hpp"

class FpsCamera: public Camera{

public:
	FpsCamera();
	FpsCamera(glm::vec3 cameraPosition, glm::vec3 cameraDirection, float FOV, float aspectRatio, glm::vec2 nearFarPlane);
	virtual ~FpsCamera();

	void MoveUpDown(float distance);
	void StrafeLeftRight(float distance);
	void MoveForwardBackward(float distance);
	void SetPostion(glm::vec3 position, glm::vec3 viewDirection);
	void ChangeCameraPitch(float angle);
	void ChangeCameraYaw(float angle);



protected:

private:
	float pitchAngle;
	float yawAngle;

};
#endif