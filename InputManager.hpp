#ifndef INPUT_MANAGER_HPP
#define INPUT_MANAGER_HPP


#include "SDL.h"
#include <vector>
#include <map>

#include "Utils.hpp"
#include "Gamepad.hpp"
#include "Keyboard.hpp"
#include  "Mouse.hpp"


class InputManager {

public:

	enum class REQUIRED_REACTION : int {
		QUIT,
		RESIZE,
		NONE
	};

	InputManager();
	~InputManager();
	REQUIRED_REACTION ProcessAllInputs(std::vector<SDL_Event>);
	Keyboard* GetKeyboard();
	Mouse* GetMouse();

protected:
	bool ProcessWindowEvent(SDL_WindowEvent*);
	void ProcessKeyboardKey(SDL_KeyboardEvent*);
	void ProcessMouseMotion(SDL_MouseMotionEvent*);
	void ProcessMouseButton(SDL_MouseButtonEvent*);
	void ProcessMouseWheel(SDL_MouseWheelEvent*);
	void ProcessGamepadAxisMotion(SDL_ControllerAxisEvent*);
	void ProcessGamepadButton(SDL_ControllerButtonEvent*);

	bool CreateNewGamePad(int controllerID);
	void DestroyGamePad(int controllerID);
	bool NewJoyDevice(int joyID);
	void DestroyJoyDevice(int joyID);

protected:
	std::map<int, Gamepad*> Gamepads;
	Mouse* mouse = nullptr;
	Keyboard* keyboard = nullptr;
};

#endif