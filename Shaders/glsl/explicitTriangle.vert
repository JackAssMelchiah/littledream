#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
    mat4 m;
    mat4 v;
    mat4 p;
} ubo;

layout(location = 0) in vec3 vertex;
layout(location = 1) in vec3 color;

layout(location = 0) out vec3 fragColor;


void main() {

    vec3 flipped = vertex;
    flipped.y *= -1;

    gl_Position = ubo.p * ubo.v * ubo.m * vec4(flipped, 1.0);
    fragColor = color;
}
