#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <glm.hpp>
#include <string>

#include <memory>
#include "Time.hpp"
#include "InputManager.hpp"
#include "FpsCamera.hpp"
#include "Window.hpp"


namespace VulkanRenderer {

	class Device;
	class VKGenericBuffer;
	class VKUniformBuffer;
}


class Renderer: public Builder{
public:
	
	Renderer();
	
	
	virtual ~Renderer() override;
	
	
	void Loop();

public:

	enum class MAIN_INPUT_TYPE { NONE, QUIT };


protected:


	bool _Initialize() override;


	bool _Build() override;


	virtual void Update(double frameTime);


	void Render();
		

	MAIN_INPUT_TYPE HandleInput(std::vector<SDL_Event>&, double);


	void HandleTime(double& frameTime);


protected:
		
	//! Window with vulkan capabilites
	VulkanRenderer::Window window;

	//! Keyboard, mouse, gamepad handling
	InputManager im;

	//! Camera
	FpsCamera camera;

	//! For getting frame times
	Time timer;

	//! Vulkan device
	std::unique_ptr<VulkanRenderer::Device> device;

	//! vertex buffer
	std::unique_ptr<VulkanRenderer::VKGenericBuffer> vbo;

	//! uniform buffer
	std::unique_ptr<VulkanRenderer::VKUniformBuffer> ubo;

	//! model mesh - vertices
	std::vector<glm::vec3> vertices;

	//! model colors
	std::vector<glm::vec3> colors;

};




#endif // ! WATER_RENDERER_HPP
