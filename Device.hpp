#ifndef _DEVICE_HPP
#define _DEVICE_HPP

#include <string>
#include <vector>
#include <map>
#include <glm.hpp>
#include <vulkan/vulkan.hpp>

#include "Builder.hpp"
#include "Utils.hpp"
#include "FixedPipelineSettings.hpp";

namespace VulkanRenderer {
    //unique pointer needs full definition, so forward declaration wont work
    class Window;
    class SwapChain;
    class Shader;
    class VertexPullerSettings;
    class RasterizerSettings;
    class InputAssemblerSettings;
    class ViewportSCizzorSettings;
    class DepthStencilSettings;
    class MultisampleSettings;
    class ColorBlendSettings;
    class VKGenericBuffer;

    class Device : public Builder {
        friend class VKGenericBuffer;
        friend class VKUniformBuffer;
    public:

        Device(Window* window, std::vector<char const*> const& windowExtensions);

        ~Device();

        void Render();

        void Resize();


        vk::Device* operator()() {

            return &this->device.get();
        }

        //! DrawCommand which records need to know buffer and triangle count - therefore needs to be supplied before Build()
        void SetBuffer(VKGenericBuffer* buffer, uint32_t vertexCount);

        //! Draws use uniform buffer as well - single buffer goes as multiple for each swapchain image...
        void SetBuffer(VKUniformBuffer* buffer);


    protected:

        bool _Build() override;


        bool _Initialize() override;

        
        void SetVertexCount(uint32_t count) { this->vertexCount = count; }

        //! Fills vector with all valid wanted validation layers
        void GetValidationLayers(std::vector<char const*>& validationLayers) const;

        //! Fills vector with all valid wanted extensions
        void GetInstanceExtensions(std::vector<char const*>& extensions) const;

        //! Checks if physical device supports all device extensions specified in extensions parameter
        bool DeviceExtensionsSupported(vk::PhysicalDevice& device, std::vector<char const*>& extensions) const;

        //! Creates swapchain from this device and from old cached swapchain if present
        void Swapchain(vk::SwapchainKHR* swapchain);

        //! Creates image view for swapchain
        void SwapchainImageView();

        //! Builds up a new shader module from Shader class, which just holds binary source from disk
        void NewShaderStage(VulkanRenderer::Shader const& shader, std::vector<vk::PipelineShaderStageCreateInfo>& shaderStageInfoStructs, std::vector<vk::UniqueShaderModule>& shaderModules);

        //! Generates new pipeline layout 
        void PipelineLayout();

        //! Creates render pass, which the renderer then uses - currently only one render pass is created
        void RenderPass();

        //! Builds new graphics pipeline from objects stored in device
        void Pipeline(std::vector<vk::PipelineShaderStageCreateInfo>& shaderStages);

        //! Creates default framebuffer
        void DefaultFramebuffer();

        //! Builds command pool, from which commands are later alocated
        void CommandPool();

        //! Creates command buffer from command pool
        void CommandBuffer();

        //! Creates renderpass and does all the draw
        void RecordRenderPass();

        //! Creates semapthore
        void Semaphores();

        //! Swaphain recreation - typically when window is resized
        void RefreshSwapchain(std::vector<vk::PipelineShaderStageCreateInfo>& shaderInfoStructs, vk::SwapchainKHR* oldSwapchain);

        //! Creates desciptor sets - basically tells pipeline that want to bind uniforms, images, etc 
        void DescriptorSetLayout();

        //! Creates allocator for description sets
        void DescriptorPool();

        //! Creates descriptor set from builded descriptor pool with required size
        void DescriptorSet(unsigned int descriptorByteSize);

    protected:

        //! helps qrouping que and its indice
        using QueWithIndice = struct {

            //! ques are basically in array, so this is the index to que in device
            unsigned int indice;

            //! once retrieved from device, its stored here
            vk::Queue que;
        };


        /***** for instance*****/
        const std::string appName;

        const std::string engineName;

        const glm::uvec3 appVersion;

        const glm::uvec3 engineVersion;

        const glm::uvec3 apiVersion;
        /********************************/

        //! Created full vulkan instance
        vk::UniqueInstance instance;

        //! Physical device
        vk::PhysicalDevice physicalDevice;

        //! stores things such as swapchain, etc...
        Window* window;

        //! surface for window
        vk::UniqueSurfaceKHR windowSurface;

        //! Created logical device
        vk::UniqueDevice device;

        //! Validation layers, which will be tested if available
        std::vector<char const*> const wantedValidationLayers;

        //! Instance Extensions, which will be tested if available
        std::vector<char const*> wantedInstanceExtensions;

        //! Extension from window needed to render images
        std::vector<char const*> const windowExtensions;

        //! Device Extensions, which will be tested if available
        std::vector<char const*> wantedDeviceExtensions;

        //! just helper to identify which que is for what
        enum class QueType : int {
            QUE_GRAPHICS,
            QUE_PRESENTATION
        };

        //! While it is very likely that graphics capabilites and presentation capabilites are on same queFamily, they could be different
        std::map<QueType, QueWithIndice> availableQues;

        //! image views for swapchain - used for rendering
        std::vector<vk::UniqueImageView> swapchainImageViews;

        //! current pipeline layout used -- should be multiple
        vk::UniquePipelineLayout pipelineLayout;

        //! Render pass - could be many more
        vk::UniqueRenderPass renderPass;

        //! VKFormat of surface HKR
        vk::SurfaceFormatKHR surfaceFormat;

        //! Object representing whole graphics pipeline - can be multiple, then one could swap quickly between them 
        vk::UniquePipeline pipeline;

        //! Holds all required pipeline settings for vertex puller
        std::unique_ptr<VertexPullerSettings> vertexPullerSettings;

        //! Holds all required pipeline settings for rasterizer
        std::unique_ptr<RasterizerSettings> rasterizerSettings;

        //! Holds all required pipeline settings for input asembler
        std::unique_ptr<InputAssemblerSettings> inputAssemblerSettings;

        //! Holds all required pipeline settings for viewport and scizzor test - but its dynamic - should be specified for every new drawcommand
        std::unique_ptr<ViewportSCizzorSettings> viewporScissorSettings;

        //! Holds all required pipeline settings for depth and scizzor settings test - but its dynamic - should be specified for every new drawcommand
        std::unique_ptr<DepthStencilSettings> depthStencilSettings;

        //! Holds all required pipeline settings for multisampling
        std::unique_ptr<MultisampleSettings> multisampleSettings;

        //! Holds all required pipeline settings for color blending
        std::unique_ptr<ColorBlendSettings> colorBlendSettings;

        //! Default frameBuffer - for each swapchain image there must be one
        std::vector<vk::UniqueFramebuffer> defaultFramebuffer;

        //! Command pool should be spawned only once
        vk::UniqueCommandPool commandPool;

        //! Command buffer - single one for every swapchainImage
        std::vector<vk::UniqueCommandBuffer> commandBuffer;

        //! Semaphore for swapchain image acquiring
        vk::UniqueSemaphore swapchainSemaphore;

        //! Semaphore for image rendering
        vk::UniqueSemaphore renderingSemaphore;

        //! SwapChain 
        vk::UniqueSwapchainKHR swapchain;

        //! Holds info about loaded shades - every shader info refers to 1 submodule on same index within vectors
        std::vector<vk::PipelineShaderStageCreateInfo> shadersInfo;

        //! Holds the submodules - required since pipeline can get recreated, and shadermoudels gets consumed by it
        std::vector<vk::UniqueShaderModule> shaderModules;

        //! buffer used in rendering
        VKGenericBuffer* vertexBuffer;

        //! buffer used in rendering
        VKUniformBuffer* uniformBuffer;

        //!...
        uint32_t vertexCount;

        //! descriptor layout
        vk::UniqueDescriptorSetLayout descriptorSetLayout;

        //! descriptors 
        std::vector<vk::UniqueDescriptorSet> descriptorSets;

        //descriptor pool
        vk::UniqueDescriptorPool descriptorPool;
    };
}



#endif
