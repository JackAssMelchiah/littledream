#ifndef BUILDER_HPP
#define BUILDER_HPP

//! Class serves as wrapper which stores info if Initialize and Build method have been called
class Builder {

public:
	
	Builder(): initialized(false), builded(false) {};
	
	virtual ~Builder() {};

	//! Wrapper method, calls internal initialize implementation of derived class
	bool Initialize() {
		
		this->initialized =_Initialize();
		return this->initialized;
	};

	//! Wrapper method, calls internal build implementation of derived class
	bool Build() {

		this->builded =_Build();
		return this->builded;
	};

	//! Querry if object is builded or not
	bool isBuilded() {
		return this->builded &&  this->initialized;
	}

	//! Querry if object is Initialized or not
	bool isInitialized() {
		return this->initialized;
	}

protected:

	//! Every subclass expects to implement it�s _Build method which gets called in Build();
	virtual bool _Build() = 0;

	//! Every subclass expects to implement it�s _Initialize method which gets called in Initialize();
	virtual bool _Initialize() = 0;

private:
	bool initialized;
	bool builded;
};


#endif // BUILDER_HPP
