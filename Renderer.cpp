#include "Renderer.hpp"
#include "Device.hpp"
#include "VKGenericBuffer.hpp"
#include "VKUniformBuffer.hpp"
#include "Shape.hpp"


Renderer::Renderer() :
	window({ 1280, 720 }, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE),
	camera(glm::vec3(0.f,-2.6f,+8.f), glm::vec3(0.f,0.f,-1.f), 75.f, 16/9.f, glm::vec2(0.1f, 1000.f)),
    timer() {


	this->camera.ChangeCameraPitch(-20.f);

}



Renderer::~Renderer() {

	this->device->operator()()->waitIdle();

	//before device gets destroyed, other vulkan objects must be as well
	this->vbo->UnBuild(); //frees vulkan alocated things in buffer
	this->ubo->UnBuild();
}


/* operation order is mandatory */
bool Renderer::_Initialize() {

	auto windowVulkanExtensions = std::vector<char const*>();

	bool sucess = this->window.Initialize();

	this->window.GetExtensions(windowVulkanExtensions);

	//init device
	this->device = std::unique_ptr<VulkanRenderer::Device>(new VulkanRenderer::Device(&this->window, windowVulkanExtensions));
	sucess &= this->device->Initialize();

	//build up geometry
	Shape::Triangle(this->vertices, this->colors);

	//init buffer
	auto verticesSize = this->vertices.size() * sizeof(glm::vec3);
	this->vbo = std::unique_ptr<VulkanRenderer::VKGenericBuffer>(new VulkanRenderer::VKGenericBuffer(this->device.get(), verticesSize * 2));
	sucess &= this->vbo->Initialize();

	//ubo buffer - will be used for MVP matrices - 3* 64bytes
	auto swapchainCount = 3;
	this->ubo = std::unique_ptr<VulkanRenderer::VKUniformBuffer>(new VulkanRenderer::VKUniformBuffer(this->device.get(), swapchainCount, 3 * sizeof(glm::mat4)));
	sucess &= this->ubo->Initialize();

	//copy the vertex data into buffer
	this->vbo->BufferData(this->vertices.data(), this->vbo->GetFreeMemoryOffset(), verticesSize);
	this->vbo->BufferData(this->colors.data(), this->vbo->GetFreeMemoryOffset(), verticesSize);
	 
	//dont copy data - should be done in the update method 

	return sucess;
}


/* Careful - mandatory execution order */
bool Renderer::_Build() {

	bool sucess = this->window.Build();

	//VBO needs to be built before device - it creates all nescessary vulkan buffer structs... and copies data to it
	sucess &= this->vbo->Build();
	if (!sucess) {
		return false;
	}
	//after build has been completed, give nescessary things back to device
	this->device->SetBuffer(this->vbo.get(), this->vertices.size());

	//now UBO - build it
	sucess &= this->ubo->Build();
	if (!sucess) {
		return false;
	}
	//after build has been completed, give nescessary things back to device
	this->device->SetBuffer(this->ubo.get());

	//so it can be builded properly and record render passes on the provided buffers...
	sucess &= this->device->Build();

	return sucess;
}



/* Updates uniform buffer */
void Renderer::Update(double frameTime) {

	auto modelMatrix = glm::mat4();
	auto viewMatrix = this->camera.GetView();
	auto projectionMatrix = this->camera.GetProjection();
	projectionMatrix[2][2] *= -1; //compensate for vulkan having flipped y coord
	projectionMatrix[1][1] *= -1; //compensate for vulkan having flipped y coord

	auto matrixSize = sizeof(glm::mat4);

	this->ubo->BufferData(&modelMatrix, 0, matrixSize);
	this->ubo->BufferData(viewMatrix, 1 * matrixSize, matrixSize);
	this->ubo->BufferData(projectionMatrix, 2 * matrixSize, matrixSize);

	this->ubo->BufferToGPU();
}


void Renderer::Render() {

	this->device->Render();
}


/*Actualizes input, updates camera*/
Renderer::MAIN_INPUT_TYPE Renderer::HandleInput(std::vector<SDL_Event>& events, double frameTime) {

	//process events from outside, and get them for GUI, tells also if should quit
	auto shouldDo = this->im.ProcessAllInputs(events);
	
	if (shouldDo == InputManager::REQUIRED_REACTION::QUIT) {
		return MAIN_INPUT_TYPE::QUIT;
	}
	else if (shouldDo == InputManager::REQUIRED_REACTION::RESIZE) {
		//refresh in window its size
		this->window.RefreshSize();
		//now that window has been refreshed, lets update device and recreate what must be recreated
		this->device->Resize();
	}


	Keyboard* keyboard = this->im.GetKeyboard();
	Mouse* mouse = this->im.GetMouse();

	if (keyboard->GetKeyState(SDLK_ESCAPE)) {
		return MAIN_INPUT_TYPE::QUIT;
	}

	auto amountToMove(5.f * frameTime);

	//fps cam input
	if (keyboard->GetKeyState(SDLK_w)) {
		this->camera.MoveForwardBackward(amountToMove);
	}
	else if (keyboard->GetKeyState(SDLK_s)) {
		this->camera.MoveForwardBackward(-amountToMove);
	}
	else if (keyboard->GetKeyState(SDLK_a)) {
		this->camera.StrafeLeftRight(-amountToMove);
	}
	else if (keyboard->GetKeyState(SDLK_d)) {
		this->camera.StrafeLeftRight(amountToMove);
	}
	else if (keyboard->GetKeyState(SDLK_LCTRL)) {
		this->camera.MoveUpDown(amountToMove);
	}
	else if (keyboard->GetKeyState(SDLK_SPACE)) {
		this->camera.MoveUpDown(-amountToMove);
	}


	glm::vec2 mousecoords = mouse->GetMouseMotionAcceleration();
	Mouse::MouseWheel wheelState = mouse->GetMouseWheelState();

	this->camera.ChangeCameraYaw(mousecoords.x * amountToMove);
	this->camera.ChangeCameraPitch(-mousecoords.y * amountToMove);

	Mouse::MousePress mousePressL = mouse->GetMouseButtonState(Mouse::MouseButton::BUTTON_LEFT);
	Mouse::MousePress mousePressR = mouse->GetMouseButtonState(Mouse::MouseButton::BUTTON_RIGHT);

	if (mousePressL.press) {
	}
	if (mousePressR.press) {
	}

	this->camera.UpdateCamera();
	return MAIN_INPUT_TYPE::NONE;
}


/*Actualizes time*/
void Renderer::HandleTime(double &frameTime) {

	//timers
	frameTime = this->timer.GetTime() * 0.001;
	if (frameTime < 0.00001) { //if some err
		frameTime = 0.0166;
	}
	this->timer.Stop();
	this->timer.Start();
}



/*Main rendering loop*/
void Renderer::Loop() {

	int width, height;
	std::vector<SDL_Event> events;
	double frameTime = 0.;

	MAIN_INPUT_TYPE event_type = MAIN_INPUT_TYPE::NONE;

	
	while (event_type != MAIN_INPUT_TYPE::QUIT) {

		HandleTime(frameTime);
		
		event_type = HandleInput(events, frameTime);
		
		Update(frameTime);

		Render();
	}
}
