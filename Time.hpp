#ifndef TIME_HPP
#define TIME_HPP

#include <chrono>

class Time {
public:

	enum class ClockState{RUNNING, PAUSED, STOPPED};

	Time();
	~Time();

	void Start();
	void Pause();
	void Resume();
	void Stop();
	double GetTime();


private:
	static double GetMilis(std::chrono::system_clock::time_point& latter, std::chrono::system_clock::time_point& sooner);

	ClockState state;
	double pausedTime;
	std::chrono::system_clock::time_point startTimePoint;

};




#endif