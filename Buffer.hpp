#ifndef BUFFER_HPP
#define BUFFER_HPP

#include <memory>
#include <vulkan/vulkan.hpp>

namespace VulkanRenderer {


	class Device;
	
	//! Its not thread safe ya
	class Buffer {

	public:
		
		Buffer(unsigned int bytesize);

		virtual ~Buffer();
		
		//! Alocates buffer - tries to - to required bytesize - its chunk of continuous memory so large vals can and will fail 
		virtual bool Initialize();

		//! Creates all required vulkan info
		virtual bool Build();
		
		//! handle to unused memory within buffer
		unsigned int GetFreeMemoryOffset() const;
		
		//! Clearing buffer - next data will be written to buffer beggining, rewriting the old ones 
		void Clear();
		
		//! Does memcpy into this buffer
		virtual void BufferData(void* data, int offset, int size);
		
		//! Accessor for reading buffer
		void* GetBufferMemoryToRead(int offset) const;
		
		//! How much bytes is allocated for this buffer
		unsigned int GetByteSize() const;

		//! Clears vulkan part
		virtual void UnBuild();

	protected:

		//! alocated data size 
		unsigned int byteSize;

		//! points to free memory, thaks to it used memory is known
		unsigned int freeMemoryPrt;

		//! raw data handle
		void* data;
	};
}




#endif // ! BUFFER_HPP
