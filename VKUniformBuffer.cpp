#include "VKUniformBuffer.hpp"

#include <cassert>

#include "Device.hpp"


VulkanRenderer::VKUniformBuffer::VKUniformBuffer(Device* device, unsigned int swapchainCount, unsigned int bytesize) :
	VulkanRenderer::Buffer(bytesize), device(device), vulkanBuffer(swapchainCount), memoryAllocation(swapchainCount){


}


VulkanRenderer::VKUniformBuffer::~VKUniformBuffer() {

}
 
/*Initializes buffer to its size on cpu*/
bool VulkanRenderer::VKUniformBuffer::Initialize() {

	//base method invocation
	if (!Buffer::Initialize()) {
		return false;
	}

	//host visible - so the host(cpu) can directly map the device memory
	//handle coherency for us, so we dont need to flush caches, etc... - slow but easy
	this->bufferRequirements.flags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
	this->bufferRequirements.size = this->byteSize;


	return true;
}


/* Creates all needed vulkan structs and buffers content in the buffer into gpu */
bool VulkanRenderer::VKUniformBuffer::Build() {

	//base method invocation
	if (!Buffer::Build()) {
		return false;
	}

	auto VRAMIndice = GetRequiredVramIndice(this->bufferRequirements);
	if (VRAMIndice == -1) {
		return false;
	}

	auto bufferInfoStruct(vk::BufferCreateInfo{}); {
		bufferInfoStruct.usage = vk::BufferUsageFlagBits::eUniformBuffer;
		bufferInfoStruct.size = this->byteSize;
		bufferInfoStruct.sharingMode = vk::SharingMode::eExclusive; //owned by signle que
		//que params are ignored since buffer is used by single que
	}

	auto device = this->device->operator()();
	auto memoryoffset = 0;
	//allocate memory on gpu..
	auto allocateInfoStruct(vk::MemoryAllocateInfo{}); {
		allocateInfoStruct.allocationSize = this->byteSize;
		allocateInfoStruct.memoryTypeIndex = VRAMIndice;
	}

	for (auto i = 0; i < this->vulkanBuffer.size(); ++i) {
		this->vulkanBuffer[i] = device->createBufferUnique(bufferInfoStruct);
		this->memoryAllocation[i] = device->allocateMemoryUnique(allocateInfoStruct);
		device->bindBufferMemory(this->vulkanBuffer[i].get(), this->memoryAllocation[i].get(), memoryoffset);
	}
	

	

	

	////now bind the allocated memory to buffer
	//auto memoryoffset = 0;
	//

	////at this point its safe to copy data to gpu
	//auto mappedMemory = device->mapMemory(this->memoryAllocation.get(), memoryoffset, this->byteSize);
	//memcpy(mappedMemory, this->data, this->byteSize);
	//device->unmapMemory(this->memoryAllocation.get());


	return true;
}


/*Writes data to buffer, offset and size are in bytes*/
void VulkanRenderer::VKUniformBuffer::BufferData(void* data, int offset, int size) {

	//base method invocation
	Buffer::BufferData(data, offset, size);
}


/* Frees stored vulkan unique pointers explicitly, since they all need to be freed before device! */
void VulkanRenderer::VKUniformBuffer::UnBuild() {

	this->device->operator()()->waitIdle();

	//base method invocation
	Buffer::UnBuild();

	this->memoryAllocation.clear();

	this->vulkanBuffer.clear();

}


/* Returns index of memory heap which meets the requirements*/
int VulkanRenderer::VKUniformBuffer::GetRequiredVramIndice(VRAMRequirements& requirements) const {

	auto memoryProperties = this->device->physicalDevice.getMemoryProperties();

	for (auto i = 0; i < memoryProperties.memoryTypeCount; i++) {
		if ((memoryProperties.memoryTypes[i].propertyFlags & requirements.flags) == requirements.flags) {
			//if it is big enough
			if (memoryProperties.memoryHeaps[memoryProperties.memoryTypes[i].heapIndex].size >= requirements.size) {
				return i;
			}
		}
	}
	return -1;
}


/* Buffers contents to GPU memory */
void VulkanRenderer::VKUniformBuffer::BufferToGPU() {

	auto device = this->device->operator()();

	for (auto& bufferMemory : this->memoryAllocation) {
		auto dest = device->mapMemory(bufferMemory.get(), 0, this->byteSize);
		memcpy(dest, this->data, this->byteSize);
		device->unmapMemory(bufferMemory.get());
	}
}
