#ifndef VK_UNIFORM_BUFFER_HPP
#define VK_UNIFORM_BUFFER_HPP

#include <memory>
#include <vulkan/vulkan.hpp>
#include "Buffer.hpp"

namespace VulkanRenderer {


	class Device;
	
	//! Its not thread safe ya
	class VKUniformBuffer: public Buffer {

	public:
		
		using VRAMRequirements = struct {
			uint32_t size;
			vk::MemoryPropertyFlags flags;
		};

		//! Uniform buffer - one buffer per swapchain count - so it would be advisable to keep the size to minimum
		VKUniformBuffer(Device *device, unsigned int swapchainCount, unsigned int bytesize);

		//
		virtual ~VKUniformBuffer( );
		
		//! Alocates buffer - tries to - to required bytesize - its chunk of continuous memory so large vals can and will fail 
		virtual bool Initialize() override;

		//! Creates all required vulkan info
		virtual bool Build() override;
				
		//! Does memcpy into this buffer
		virtual void BufferData(void* data, int offset, int size) override;

		//! Original vulkan structure for access - 
		vk::Buffer* operator()(unsigned int i) { return &this->vulkanBuffer.at(i).get(); }

		//! Clears vulkan part
		virtual void UnBuild() override;

		//! Returns indice to memory
		int GetRequiredVramIndice(VRAMRequirements& requirements) const;

		//! Buffers cpu buffer into gpu memory (all swapchain versions) 
		void BufferToGPU();

	protected:

		//! Buffer interaction goes against this device
		Device* device;

		//! vulkan buffer object
		std::vector<vk::UniqueBuffer> vulkanBuffer;

		//! alocation
		std::vector<vk::UniqueDeviceMemory> memoryAllocation;

		//!Requirements for VRAM to meet if it want to support this buffer
		VRAMRequirements bufferRequirements;
	};
}




#endif // ! VK_UNIFORM_BUFFER_HPP
