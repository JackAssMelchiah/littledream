
/*Visual leak detector*/
//#include <vld.h>


#ifndef RESOURCE_DIR
#define RESOURCE_DIR "Resources/"
#endif

#ifndef SHADER_DIR
#define SHADER_DIR "Shaders/"
#endif



#include <SDL.h>
#include <SDL_image.h>
#include <string>

#include "Renderer.hpp"
#include "Utils.hpp"


int main(int, char* args[]);
void Close();
bool Init();


int main(int argc, char* args[]) {
	
	if (!Init()) {
		Utils::PrintOutErr("Failed to initialize!");
		Close();
		return 1;
	}

	auto renderer = new Renderer();
	
	renderer->Initialize();
	renderer->Build();
	renderer->Loop();

	delete renderer;
	Close();	
	return 0;
}



/*Initializes SDL*/
bool Init() {

	if (SDL_Init(SDL_INIT_EVERYTHING) < 0) {
		Utils::PrintOutErr("SDL init err");
		return false;
	}

	//init SLD IMAGE 
	int flags = IMG_INIT_JPG | IMG_INIT_PNG;
	int init_img = IMG_Init(flags);
	if (init_img & flags != flags) {
		Utils::PrintOutErr("cannot init JPG&PNG! Sorry.");
		return false;
	}

	
	return true;
}



/*Close*/
void Close(){

	IMG_Quit();
	SDL_Quit();
}
