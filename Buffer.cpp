#include "Buffer.hpp"

#include <cassert>
#include <vulkan/vulkan.hpp>

#include "Device.hpp"


VulkanRenderer::Buffer::Buffer(unsigned int bytesize) :
	freeMemoryPrt(0), byteSize(bytesize), data(nullptr) {
}


VulkanRenderer::Buffer::~Buffer() {

	if (this->byteSize == 0 || this->data == nullptr)
		return;

	delete[] (char*)this->data;
}


/*Clears buffer*/
void VulkanRenderer::Buffer::Clear() {

	this->freeMemoryPrt = 0;
}


/*Returs size*/
unsigned int VulkanRenderer::Buffer::GetByteSize() const{

	//if !initialized return 0

	return this->byteSize;
}


/*Initializes buffer to its size on cpu*/
bool VulkanRenderer::Buffer::Initialize() {

	this->data = (void*)new char[this->byteSize];
	if (this->data == nullptr) return false;

	return true;
}


/* Creates all needed vulkan structs and buffers content in the buffer into gpu */
bool VulkanRenderer::Buffer::Build() {

	return true;
}


/*Returns free memory offset in bytes*/
unsigned int VulkanRenderer::Buffer::GetFreeMemoryOffset() const {

	return this->freeMemoryPrt;
}


/*Writes data to buffer, offset and size are in bytes*/
void VulkanRenderer::Buffer::BufferData(void* data, int offset, int size) {

	assert(this->data != nullptr);

	//move end buffer if necessary
	if (offset + size > this->freeMemoryPrt) {
		this->freeMemoryPrt = offset + size;
	}
	
	void* dest_ptr = (void*)(((char*)this->data) + offset);
	memcpy(dest_ptr, data, size);
}


void* VulkanRenderer::Buffer::GetBufferMemoryToRead(int offset) const {

	return (void*)(((char*)this->data) + offset);
}



void VulkanRenderer::Buffer::UnBuild() {

}