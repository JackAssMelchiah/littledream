#ifndef GAMEPAD_HPP_
#define GAMEPAD_HPP_

#include <map>

#include "SDL.h"
#include "glm.hpp"
#include "Utils.hpp"

class Gamepad{
public:
	enum class AXIS{LEFT_X, LEFT_Y, RIGHT_X, RIGHT_Y, LEFT_TRIG, RIGHT_TRIG};
	Gamepad(SDL_GameController*);
	~Gamepad();

	//void ProcessEvent(SDL_Event*);
	void ProcessEvent(SDL_ControllerAxisEvent*);
    void ProcessEvent(SDL_ControllerButtonEvent*);
	void SetDeadZoneAxis(float percent);
	float GetDeadZoneAxis();
	void SetDeadZoneTrigger(float percent);
	float GetDeadZoneTrigger();

	int GetAxisValue(Gamepad::AXIS);

	void SetName(std::string);
	std::string GetName();
	void Rumble(float, int);

private:

    std::string AxisToString(SDL_GameControllerAxis axis);

	SDL_GameController* sdlSelf = nullptr;
	SDL_Haptic* hapticSelf = nullptr;

	int deadzoneAxis;
	int deadzoneTriggers;

	std::string name;
    std::map <int, bool> buttons;
	std::map <int, int> axis;
};

#endif // !GAMEPAD_HPP_