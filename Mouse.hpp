#ifndef MOUSE_HPP
#define MOUSE_HPP

#include "SDL.h"
#include <map>

#include "Utils.hpp"



class Mouse {
public:
	enum class MouseWheel { MOVEUP, MOVEDOWN, STALL };
	enum class MouseButton { BUTTON_LEFT, BUTTON_MIDDLE, BUTTON_RIGHT, BUTTON_X1, BUTTON_X2 };
	typedef struct { bool press; bool doublePress; } MousePress;
	
	Mouse(bool trapMouseInWindow = true);
	~Mouse();

	void ProcessEvent(SDL_MouseMotionEvent* e);
	void ProcessEvent(SDL_MouseButtonEvent* e);
	void ProcessEvent(SDL_MouseWheelEvent* e);
	Mouse::MouseWheel GetMouseWheelState();
	Mouse::MousePress GetMouseButtonState(Mouse::MouseButton);
	glm::ivec2 GetMouseMotionAcceleration();
	glm::ivec2 GetMousePosition();

private:

	std::map<int, MousePress> mouseButtons;
	MouseWheel wheel;
	glm::ivec2 mouseMotionAcceleration;
	glm::ivec2 absoluteMouseCoords;


};
#endif // !MOUSE_HPP
