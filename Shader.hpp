#ifndef _SHADER_HPP_
#define _SHADER_HPP_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <vulkan/vulkan.hpp>

namespace VulkanRenderer {
	
	class Shader {
	public:

		//! Only supported shaders right now
		enum class ShaderType : int {
			VERTEX,
			FRAGMENT,
			COMPUTE
		};

		struct ShaderSource {
			std::string fullPath;
			ShaderType type;
		};

		//! Loaded shader
		struct ShaderBinary {
			std::vector<char> srcCode;
			vk::ShaderStageFlagBits shaderTypeVK;
		};


		Shader(std::vector<ShaderSource>& const shaders) : shaderInputs(shaders) {}

		~Shader() {}

		//! Getter for loaded shaders
		std::vector<ShaderBinary> const& GetShaderBinaries() const { return this->shaderBinaries; }

		//! Loads shaders from disk
		bool LoadShaders() {
			
			this->shaderBinaries.resize(this->shaderInputs.size());
			for (auto i = 0; i < this->shaderInputs.size(); ++i) {
				auto fileStream = std::ifstream(shaderInputs.at(i).fullPath, std::ifstream::binary);
				if (!fileStream) {
					assert(!"fileStream bad");
					return false;
				}
				

				fileStream.seekg(0, fileStream.end);
				auto length = fileStream.tellg();
				fileStream.seekg(0, fileStream.beg);
				
				shaderBinaries[i].srcCode.resize(length);
				fileStream.read(shaderBinaries[i].srcCode.data(), length);
				fileStream.close();	
				shaderBinaries[i].shaderTypeVK = ShaderTypeToTypeVK(shaderInputs[i].type);
			}
			return true;
		}


	protected:
		vk::ShaderStageFlagBits ShaderTypeToTypeVK(ShaderType type) {
			if (type == ShaderType::VERTEX) {
				return vk::ShaderStageFlagBits::eVertex;
			}
			else if (type == ShaderType::FRAGMENT) {
				return vk::ShaderStageFlagBits::eFragment;
			}
			else if (type == ShaderType::COMPUTE) {
				return vk::ShaderStageFlagBits::eCompute;
			}
			return vk::ShaderStageFlagBits::eAll;
		}


	protected:

		//! stores locations of shaders
		std::vector<ShaderSource> shaderInputs;
		//! stores loaded shader binaries 
		std::vector<ShaderBinary> shaderBinaries;
	};
}
#endif