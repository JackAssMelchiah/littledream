#include "VKGenericBuffer.hpp"
#include "Device.hpp"


VulkanRenderer::VKGenericBuffer::VKGenericBuffer(Device* device, unsigned int bytesize) :
	VulkanRenderer::Buffer(bytesize), device(device) {
}


VulkanRenderer::VKGenericBuffer::~VKGenericBuffer() {

}


/*Initializes buffer to its size on cpu*/
bool VulkanRenderer::VKGenericBuffer::Initialize() {

	//base method invocation
	if (!VulkanRenderer::Buffer::Initialize()) {
		return false;
	}

	//host visible - so the host(cpu) can directly map the device memory
	//handle coherency for us, so we dont need to flush caches, etc... - slow but easy
	this->bufferRequirements.flags = vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent;
	this->bufferRequirements.size = this->byteSize;

	return true;
}


/* Creates all needed vulkan structs and buffers content in the buffer into gpu */
bool VulkanRenderer::VKGenericBuffer::Build() {

	//base method invocation
	if (!VulkanRenderer::Buffer::Build()) {
		return false;
	}

	auto VRAMIndice = GetRequiredVramIndice(this->bufferRequirements);
	if (VRAMIndice == -1) {
		return false;
	}

	auto bufferInfoStruct(vk::BufferCreateInfo{}); {
		bufferInfoStruct.usage = vk::BufferUsageFlagBits::eVertexBuffer;
		bufferInfoStruct.size = this->byteSize;
		bufferInfoStruct.sharingMode = vk::SharingMode::eExclusive; //owned by signle que
		//que params are ignored since buffer is used by single que
	}
	
	auto device = this->device->operator()();
	
	this->vulkanBuffer = device->createBufferUnique(bufferInfoStruct);

	//allocate memory on gpu..
	auto allocateInfoStruct(vk::MemoryAllocateInfo{}); {
		allocateInfoStruct.allocationSize = this->byteSize;
		allocateInfoStruct.memoryTypeIndex = VRAMIndice;
	}
	
	this->memoryAllocation = device->allocateMemoryUnique(allocateInfoStruct);

	//now bind the allocated memory to buffer
	auto memoryoffset = 0;
	device->bindBufferMemory(this->vulkanBuffer.get(), this->memoryAllocation.get(), memoryoffset);

	//at this point its safe to copy data to gpu
	auto mappedMemory = device->mapMemory(this->memoryAllocation.get(), memoryoffset, this->byteSize);
	memcpy(mappedMemory, this->data, this->byteSize);
	device->unmapMemory(this->memoryAllocation.get());
}


/* Returns index of memory heap which meets the requirements*/
int VulkanRenderer::VKGenericBuffer::GetRequiredVramIndice(VRAMRequirements& requirements) const {

	auto memoryProperties = this->device->physicalDevice.getMemoryProperties();

	for (auto i = 0; i < memoryProperties.memoryTypeCount; i++) {
		if ((memoryProperties.memoryTypes[i].propertyFlags & requirements.flags) == requirements.flags) {
			//if it is big enough
			if (memoryProperties.memoryHeaps[memoryProperties.memoryTypes[i].heapIndex].size >= requirements.size) {
				return i;
			}
		}
	}
	return -1;
}


/* Frees stored vulkan unique pointers explicitly, since they all need to be freed before device! */
void VulkanRenderer::VKGenericBuffer::UnBuild() {
	
	//! base method invocation 
	Buffer::UnBuild();

	this->memoryAllocation.reset();

	this->vulkanBuffer.reset();
}