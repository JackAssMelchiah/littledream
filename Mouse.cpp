#include "Mouse.hpp"


Mouse::Mouse(bool trapMouseInWindow) {

	if(trapMouseInWindow)
		SDL_SetRelativeMouseMode(SDL_TRUE);
	this->absoluteMouseCoords = glm::ivec2(0.f);
	this->mouseMotionAcceleration = glm::ivec2(0.f);
	this->wheel = Mouse::MouseWheel::STALL;
}


Mouse::~Mouse() {}


/*Process mouse motion */
void Mouse::ProcessEvent(SDL_MouseMotionEvent* e) {

	//absolute position of mouse, always will stay there
	this->absoluteMouseCoords = glm::ivec2(e->x, e->y);
	//its just an acceleration, will be set 0 after accessing it;
	this->mouseMotionAcceleration = glm::ivec2(glm::clamp(e->xrel,-10, 10), glm::clamp(e->yrel * -1, -10, 10));
}


/*Processes button press on mouse*/
void Mouse::ProcessEvent(SDL_MouseButtonEvent * e) {

	MousePress press;
	press.doublePress = false;
	press.press = false;

	if (e->state == SDL_PRESSED) {
		press.press = true;
		if (e->clicks == 2) {
			press.doublePress = true;
		}
	}
	
	this->mouseButtons[e->button] = press;
}


/*Processes mouse wheel event*/
void Mouse::ProcessEvent(SDL_MouseWheelEvent * e) {

	if (e->y == 1) {
		this->wheel = MouseWheel::MOVEUP;
	}
	else { this->wheel = MouseWheel::MOVEDOWN; }

}


/*Returns mouse wheel's current state
*if was modified->either MOVEUP or MOVEDOWN, then set it to stall, since now its not moving
*/
Mouse::MouseWheel Mouse::GetMouseWheelState() {

	MouseWheel retval = this->wheel;
	this->wheel = MouseWheel::STALL;
	return retval;
}


/*Returns state of selected mouse button*/
Mouse::MousePress Mouse::GetMouseButtonState(Mouse::MouseButton btn){

	int key;
	switch (btn) {
	case Mouse::MouseButton::BUTTON_LEFT: {
		key = SDL_BUTTON_LEFT;
		break;
	}
	case Mouse::MouseButton::BUTTON_MIDDLE: {
		key = SDL_BUTTON_MIDDLE;
		break;
	}
	case Mouse::MouseButton::BUTTON_RIGHT: {
		key = SDL_BUTTON_RIGHT;
		break;
	}
	case Mouse::MouseButton::BUTTON_X1: {
		key = SDL_BUTTON_X1;
		break;
	}
	case Mouse::MouseButton::BUTTON_X2: {
		key = SDL_BUTTON_X2;
		break;
	}
	default:
		break;
	}
	return this->mouseButtons[key];
}


/*Returns acceleration of mouse in both axis from last motion*/
glm::ivec2 Mouse::GetMouseMotionAcceleration() {
	
	//delete acceleration after reading it
	glm::ivec2 retval = this->mouseMotionAcceleration;

	this->mouseMotionAcceleration = glm::ivec2(0.f);
	return retval;
}


/*Returns absolute coords in pixels of mouse*/
glm::ivec2 Mouse::GetMousePosition() {
	
	return this->absoluteMouseCoords;
}
