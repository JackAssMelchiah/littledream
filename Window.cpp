#include "Window.hpp"
#include <SDL_vulkan.h>


VulkanRenderer::Window::Window(glm::ivec2 dimmensions, Uint32 flags):dimmensions(dimmensions), flags(flags), window(nullptr) {

}


bool VulkanRenderer::Window::_Initialize() {

	this->window = SDL_CreateWindow("Vulkan Renderer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, this->dimmensions.x, this->dimmensions.y, this->flags | SDL_WINDOW_VULKAN);
	if (!this->window) {
		return false;
	}
	return true;
}



void VulkanRenderer::Window::GetRect(vk::Rect2D& rectangle) {

	rectangle.offset = vk::Offset2D{ 0, 0 };
	rectangle.extent = vk::Extent2D{ uint32_t(this->dimmensions.x), uint32_t(this->dimmensions.y) };
}


bool VulkanRenderer::Window::_Build() {

	return true;
};


void VulkanRenderer::Window::GetExtensions(std::vector<char const*>& extensions) const {

	extensions.clear();

	auto windowExtensionsCount = unsigned int(0);
	auto success = SDL_Vulkan_GetInstanceExtensions(this->window, &windowExtensionsCount, nullptr);
	extensions.resize(windowExtensionsCount);
	success = SDL_Vulkan_GetInstanceExtensions(this->window, &windowExtensionsCount, extensions.data());
	assert(success);
}


void VulkanRenderer::Window::RefreshSize() {

	//querry real window size
	SDL_GetWindowSize(this->window, &this->dimmensions.x, &this->dimmensions.y);
}


VulkanRenderer::Window::~Window(){

	SDL_DestroyWindow(this->window);
}


