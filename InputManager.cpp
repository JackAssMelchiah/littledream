#include "InputManager.hpp"


/*Process all input devices*/
InputManager::InputManager(){

	SDL_GameControllerEventState(SDL_ENABLE);
	this->keyboard = new Keyboard();
	this->mouse = new Mouse(false);

}


/*Processes all inputs, sends them into their own devices*/
InputManager::REQUIRED_REACTION InputManager::ProcessAllInputs(std::vector<SDL_Event> events){

	SDL_Event event;
	SDL_Event eventCopy;

	

	/*Divides events into categories, and saves the nessesary ones for gui*/
    while (SDL_PollEvent(&event)){
		eventCopy = event;
		events.push_back(eventCopy);
		switch (event.type){
		
		case(SDL_WINDOWEVENT):{
			
			if (ProcessWindowEvent(&event.window)) {
				return REQUIRED_REACTION::RESIZE;
				break;
			}
			break;
		}
		
		case(SDL_KEYDOWN): {
			ProcessKeyboardKey(&event.key);
			break;
		}
		case(SDL_KEYUP): {
			ProcessKeyboardKey(&event.key);
			break;
		}

		case(SDL_MOUSEMOTION): {
			ProcessMouseMotion(&event.motion);
			break;
		}

		case(SDL_MOUSEBUTTONDOWN): {
			ProcessMouseButton(&event.button);
			break;
		}
		case(SDL_MOUSEBUTTONUP): {
			ProcessMouseButton(&event.button);
			break;
		}
		case(SDL_MOUSEWHEEL): {
			ProcessMouseWheel(&event.wheel);
			break;
		}

		/*case(SDL_JOYDEVICEADDED): {
			Utils::PrintOut("new JOY");

			if (!NewJoyDevice(event.jdevice.which)) {
				Utils::PrintOutWar("Unable to create new gamepad");
			}
			break;
		}
		case(SDL_JOYDEVICEREMOVED): {
			Utils::PrintOut("removing JOY");

			DestroyJoyDevice(event.jdevice.which);
			break;
		}*/
		case(SDL_CONTROLLERDEVICEADDED): {
			Utils::PrintOut("Adding new gamepad");

			if (!CreateNewGamePad(event.cdevice.which)) {
				Utils::PrintOutWar("Unable to create new gamepad");
			}
			break;
		}
		case(SDL_CONTROLLERDEVICEREMOVED): {
			Utils::PrintOut("Removing gamepad");
			DestroyGamePad(event.cdevice.which);
			break;
		}
		case(SDL_CONTROLLERDEVICEREMAPPED): {
			Utils::PrintOutWar("Remmaping gamepad, which is currently not supported");
			break;
		}
		case(SDL_CONTROLLERAXISMOTION): {
			ProcessGamepadAxisMotion(&event.caxis);
			break;
		}
		case(SDL_CONTROLLERBUTTONDOWN): {
			ProcessGamepadButton(&event.cbutton);
			break;
		}
		case( SDL_CONTROLLERBUTTONUP): {
			ProcessGamepadButton(&event.cbutton);
			break;
		}
		case(SDL_QUIT): {
			return REQUIRED_REACTION::QUIT;
			break;
		}
		default:
			break;
		}
    }
	return REQUIRED_REACTION::NONE;
}


/*Returns true if resize was called*/
bool InputManager::ProcessWindowEvent(SDL_WindowEvent *e){

	if (e->event == SDL_WINDOWEVENT_RESIZED) {
		return true;
	}
	return false;
}


/*Processes keyboard key press*/
void InputManager::ProcessKeyboardKey(SDL_KeyboardEvent *e){

	this->keyboard->ProcessEvent(e);
}


/*Processes Mouse motion*/
void InputManager::ProcessMouseMotion(SDL_MouseMotionEvent *e){

	this->mouse->ProcessEvent(e);
}


/*Processes mouse Button press*/
void InputManager::ProcessMouseButton(SDL_MouseButtonEvent *e){

	this->mouse->ProcessEvent(e);
}


/*Processes mouse wheel event*/
void InputManager::ProcessMouseWheel(SDL_MouseWheelEvent *e){

	this->mouse->ProcessEvent(e);
}


/*Gamepad Axis Processing*/
void InputManager::ProcessGamepadAxisMotion(SDL_ControllerAxisEvent *e){
	this->Gamepads[e->which]->ProcessEvent(e);
}


/*Gamepad button processing*/
void InputManager::ProcessGamepadButton(SDL_ControllerButtonEvent *e){
	
	this->Gamepads[e->which]->Rumble(1.f, 1000);

	this->Gamepads[e->which]->ProcessEvent(e);
}


/*Creates new gamepad*/
bool InputManager::CreateNewGamePad(int controllerID){

	//open the device
	SDL_GameController* controller = SDL_GameControllerOpen(controllerID);
	if (controller == nullptr) {
		return false;
	}

	Gamepad* g = new Gamepad(controller);
	g->SetName(SDL_GameControllerNameForIndex(controllerID));
	g->SetDeadZoneAxis(0.0f);
	g->SetDeadZoneTrigger(0.0f);

	this->Gamepads.insert(std::pair<int, Gamepad*>(controllerID, g));
	
	//Utils::PrintOut(string("CONTROLLER") + g->GetName()+("ID :") + std::to_string(controllerID));
	return true;
}


/*Destroys gamepad, frees it from memory*/
void InputManager::DestroyGamePad(int controllerID){

	//get him and free him, but later smart pointer
	delete this->Gamepads[controllerID];
	this->Gamepads.erase(controllerID);
}



Keyboard* InputManager::GetKeyboard() {

	return this->keyboard;
}

Mouse * InputManager::GetMouse() {
	return this->mouse;
}

bool InputManager::NewJoyDevice(int joyID) {
	return false;
}

/**/
void InputManager::DestroyJoyDevice(int joyID) {


}


/**/
InputManager::~InputManager(){

	if (this->mouse != nullptr) {
		delete this->mouse;
	}
	if (this->keyboard != nullptr) {
		delete this->keyboard;
	}

	for (auto gamepad : this->Gamepads) {
		delete gamepad.second;
	}
}


