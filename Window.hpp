#ifndef WINDOW_HPP
#define WINDOW_HPP

#include <SDL.h>
#include <glm.hpp>
#include <vector>
#include <vulkan/vulkan.hpp>

#include "Builder.hpp"

namespace VulkanRenderer {

	class Window : public Builder {

		friend class Device;

	public:

		Window(glm::ivec2 dimmensions, Uint32 flags);

		virtual ~Window() override;

		//! fills vector with needed extensions for window to be rendered on
		void GetExtensions(std::vector<char const*>& extensions) const;

		void RefreshSize();

	protected:

		virtual bool _Initialize() override;

		virtual bool _Build() override;

		void GetRect(vk::Rect2D& rectangle);


	protected:

		//! Dimmensions of window
		glm::ivec2 dimmensions;

		//! Added flags for window creation
		Uint32 flags;

		//! SDL_Window holder
		SDL_Window* window;

		//! Drawable rectangle of single window, which is bound to device
		vk::Rect2D windowDrawableRect;

	};
}

#endif // !WINDOW_HPP
