#include "Keyboard.hpp"

Keyboard::Keyboard() {}

Keyboard::~Keyboard() {}


/*Processes keyboard press*/
void Keyboard::ProcessEvent(SDL_KeyboardEvent* e) {

	if (e->state == SDL_PRESSED) {
		this->keyboard[e->keysym.sym] = true;
	}
	else {
		this->keyboard[e->keysym.sym] = false;
	}
}


/*Returns state of key, true for pressed
*When not pressed before, key might not be found --error? should look if doesnt exist, then false?
*/
bool Keyboard::GetKeyState(SDL_Keycode k) {

	return this->keyboard[k];
}

