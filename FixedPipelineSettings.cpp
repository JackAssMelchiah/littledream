#include "FixedPipelineSettings.hpp"

VulkanRenderer::VertexPullerSettings::VertexPullerSettings():
    bindingDesc(2), attribDesc(2){


    /*Vertex attribute*/
    //specify attachement
    this->attribDesc[0].binding = 0; // buffer on cpu is bound to 0 in cmdDraw
    this->attribDesc[0].format = vk::Format::eR32G32B32Sfloat; // ...
    this->attribDesc[0].location = 0; // location in shader which this attachment is fed to is 0 too 
    this->attribDesc[0].offset = 0; //its not interleaved struct

    //specify binding in shader
    this->bindingDesc[0].binding = 0; //buffer on cpu is bound to 0 in cmdDraw
    this->bindingDesc[0].inputRate = vk::VertexInputRate::eVertex; //feed next data per vertex shader invocation
    this->bindingDesc[0].stride = sizeof(glm::vec3); //the attribute is vec3, so for every vertex shader invocation vertex puller should pull 12 bytes from buffer
    
    /*Color attribute*/
    this->attribDesc[1].binding = 1; // buffer on cpu is bound to 1 in cmdDraw
    this->attribDesc[1].format = vk::Format::eR32G32B32Sfloat; // ...
    this->attribDesc[1].location = 1; // location in shader which this attachment is fed to is 0 too 
    this->attribDesc[1].offset = 0; //its not interleaved struct

    //specify binding in shader
    this->bindingDesc[1].binding = 1; //buffer on cpu is bound to 1 in cmdDraw
    this->bindingDesc[1].inputRate = vk::VertexInputRate::eVertex; //feed next data per vertex shader invocation
    this->bindingDesc[1].stride = sizeof(glm::vec3); //the attribute is vec3, so for every vertex shader invocation vertex puller should pull 12 bytes from buffer


    //put it all together
    this->vertexPullerInfoStruct.vertexAttributeDescriptionCount = this->attribDesc.size();
    this->vertexPullerInfoStruct.pVertexAttributeDescriptions = this->attribDesc.data();
    this->vertexPullerInfoStruct.vertexBindingDescriptionCount = this->bindingDesc.size();
    this->vertexPullerInfoStruct.pVertexBindingDescriptions = this->bindingDesc.data();
}


VulkanRenderer::RasterizerSettings::RasterizerSettings(){

    this->rasterizerInfoStruct.depthClampEnable = VK_FALSE;
    this->rasterizerInfoStruct.rasterizerDiscardEnable = VK_FALSE; //want to write
    this->rasterizerInfoStruct.polygonMode = vk::PolygonMode::eFill;
    this->rasterizerInfoStruct.lineWidth = 1.f; 
    this->rasterizerInfoStruct.cullMode = vk::CullModeFlagBits::eNone;
    this->rasterizerInfoStruct.frontFace = vk::FrontFace::eCounterClockwise; //flipping zcoord, so this needs to be CC
    this->rasterizerInfoStruct.depthBiasEnable = VK_FALSE;
    this->rasterizerInfoStruct.depthBiasClamp = 0.f;
    this->rasterizerInfoStruct.depthBiasConstantFactor = 0.f;
    this->rasterizerInfoStruct.depthBiasSlopeFactor = 0.f;
}


VulkanRenderer::InputAssemblerSettings::InputAssemblerSettings(){

    this->inputAssemblyInfoStruct.primitiveRestartEnable = VK_FALSE;
    this->inputAssemblyInfoStruct.topology = vk::PrimitiveTopology::eTriangleList;
}


VulkanRenderer::ColorBlendSettings::ColorBlendSettings() {

    this->colorBlendAttachmentsInfoStruct.alphaBlendOp = vk::BlendOp::eAdd;
    this->colorBlendAttachmentsInfoStruct.blendEnable = VK_FALSE;
    this->colorBlendAttachmentsInfoStruct.colorBlendOp = vk::BlendOp::eAdd;
    this->colorBlendAttachmentsInfoStruct.colorWriteMask = vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA;
    this->colorBlendAttachmentsInfoStruct.dstAlphaBlendFactor = vk::BlendFactor::eOne;
    this->colorBlendAttachmentsInfoStruct.srcAlphaBlendFactor = vk::BlendFactor::eOne;
    this->colorBlendAttachmentsInfoStruct.srcColorBlendFactor = vk::BlendFactor::eOne;

    
    this->colorBlendStateInfoStruct.attachmentCount = 1;
    this->colorBlendStateInfoStruct.pAttachments = &this->colorBlendAttachmentsInfoStruct;
    this->colorBlendStateInfoStruct.logicOp = vk::LogicOp::eCopy;
    this->colorBlendStateInfoStruct.logicOpEnable = VK_FALSE;

}


VulkanRenderer::MultisampleSettings::MultisampleSettings(){

    this->multisampleInfoStruct.alphaToCoverageEnable = VK_FALSE;
    this->multisampleInfoStruct.alphaToOneEnable = VK_FALSE;
    this->multisampleInfoStruct.minSampleShading = 1.f;
    this->multisampleInfoStruct.pSampleMask = nullptr;
    this->multisampleInfoStruct.rasterizationSamples = vk::SampleCountFlagBits::e1;
    this->multisampleInfoStruct.sampleShadingEnable = VK_FALSE;
}


VulkanRenderer::DepthStencilSettings::DepthStencilSettings(){


}


VulkanRenderer::ViewportSCizzorSettings::ViewportSCizzorSettings( glm::ivec2 resoultion){


    this->scissorRect.offset = vk::Offset2D{ 0, 0 };
    this->scissorRect.extent = vk::Extent2D{ uint32_t(resoultion.x), uint32_t(resoultion.y) };


    this->viewport.x = 0.f;
    this->viewport.y = 0.f;
    this->viewport.width = resoultion.x;
    this->viewport.height = resoultion.y;
    this->viewport.minDepth = 0.f;
    this->viewport.maxDepth = 1.f;


    this->viewportScissorInfoStruct.scissorCount = 1;
    this->viewportScissorInfoStruct.pScissors = &this->scissorRect;
    this->viewportScissorInfoStruct.viewportCount = 1;
    this->viewportScissorInfoStruct.pViewports = &this->viewport;
}


void VulkanRenderer::ViewportSCizzorSettings::set(glm::ivec2 resolution){


    this->scissorRect.extent = vk::Extent2D{ uint32_t(resolution.x), uint32_t(resolution.y) };

    this->viewport.width = resolution.x;
    this->viewport.height = resolution.y;


    this->viewportScissorInfoStruct.scissorCount = 1;
    this->viewportScissorInfoStruct.pScissors = &this->scissorRect;
    this->viewportScissorInfoStruct.viewportCount = 1;
    this->viewportScissorInfoStruct.pViewports = &this->viewport;
}
