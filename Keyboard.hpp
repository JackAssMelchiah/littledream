#ifndef KEYBOARD_HPP
#define KEYBOARD_HPP

#include <map>
#include <SDL.h>

class Keyboard{

public:
    Keyboard();
    ~Keyboard();
    void ProcessEvent(SDL_KeyboardEvent*);
	bool GetKeyState(SDL_Keycode k);

private:

	std::map<int, bool> keyboard;
};
#endif
