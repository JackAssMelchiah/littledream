/**********************************
* Utils.hpp
* Serves mainly as logging "service"
**for more info about class mehods, please check out .cpp file
*/

#ifndef UTILS_HPP
#define UTILS_HPP

#include <time.h>
#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <string>
#include <glm.hpp>
#include <gtc/quaternion.hpp>
#include "SDL.h"
#include "Shape.hpp"
#include <stdio.h>

using namespace std;
class Utils{

public:
	typedef unsigned int FILE_DESCRIPTOR_ID;


	/*Shifts vector left by specified amount units*/
	template <typename T> static void ShiftLeft(std::vector<T>* vector, int by) {

		rotate(vector->begin(), vector->begin() + by, vector->end());
		for (int i = vector->size() - 1; i > vector->size() - 1 - by; --i) {
			vector->at(i) = (T)0;
		}
	}

	/*Shifts vector right by specified amount units*/
	template <typename T> static void ShiftRight(std::vector<T>* vector, int by) {

		rotate(vector->begin(), vector->begin() + by, vector->end());
		for (int i = 0; i < by - 1; ++i) {
			vector->at(i) = (T)0;
		}
	}
	static unsigned int AlignMemory(unsigned int);
	static std::string VecToStr(std::vector<float>& vector);
	static void ListDir(std::string path, std::vector<std::string> result);
	static void GetWindowResolution(SDL_Window* window, int* width, int* height);
	static glm::ivec2 GetWindowResolution(SDL_Window* window);
	static void SetWindowResolution(SDL_Window* window, int width, int height);
	static void SetWindowResolution(SDL_Window* window, glm::ivec2 res);
	static std::string VecToStr(glm::vec4);
	static std::string VecToStr(glm::vec3);
	static std::string VecToStr(glm::vec2);
	static float Interpolate(float A, float B, float t);
	static glm::vec3 Interpolate(glm::vec3& A, glm::vec3& B, float t);
	static glm::quat Interpolate(glm::quat& A, glm::quat& B, float t);
	static void SplitString(string target, string split_by, std::vector<string>& ret);
	static float ConvertRange(float curr_val, float old_min, float old_max, float new_min, float new_max);
	static void GetFullResourcePath(string& abs_path);
	static void GetFullShaderPath(string& abs_path);
	static void DoIndexing(std::vector<glm::vec3>& vertices, std::vector<glm::vec2>& uvs, std::vector<glm::vec3>& normals, std::vector<int>& indices, std::vector<glm::vec3>& indexed_vert, std::vector<glm::vec2>& indexed_uvs, std::vector<glm::vec3>& indexed_normals);
	static void PrintOutErr(std::string);
	static void PrintOutWar(std::string);
	static void PrintOut(std::string);
	static void PrintOutGL(std::string);
	static bool WriteToClearFile(std::string path, void * data, unsigned int elementByteSize, unsigned int length);
	static bool WriteToExistingFile(std::string path, void * data, unsigned int elementByteSize, unsigned int length);
	static bool ReadFromFile(std::string path, int expectedElementize, void** data, unsigned int & lenght);

};
#endif